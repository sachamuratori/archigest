<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BookedexamsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BookedexamsTable Test Case
 */
class BookedexamsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bookedexams',
        'app.exams',
        'app.wards',
        'app.dailyturns',
        'app.turns',
        'app.users',
        'app.medicalrecords',
        'app.treatments',
        'app.users_medicalrecords',
        'app.orders',
        'app.articles',
        'app.suppliers',
        'app.orders_articles',
        'app.warehouses',
        'app.warehouses_articles',
        'app.users_dailyturns',
        'app.users_exams',
        'app.rooms'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Bookedexams') ? [] : ['className' => 'App\Model\Table\BookedexamsTable'];
        $this->Bookedexams = TableRegistry::get('Bookedexams', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bookedexams);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
