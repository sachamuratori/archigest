<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TreatmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TreatmentsTable Test Case
 */
class TreatmentsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.treatments',
        'app.medicalrecords',
        'app.bookedexams',
        'app.exams',
        'app.wards',
        'app.dailyturns',
        'app.turns',
        'app.users',
        'app.users_medicalrecords',
        'app.orders',
        'app.articles',
        'app.suppliers',
        'app.orders_articles',
        'app.warehouses',
        'app.warehouses_articles',
        'app.users_dailyturns',
        'app.users_exams',
        'app.rooms'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Treatments') ? [] : ['className' => 'App\Model\Table\TreatmentsTable'];
        $this->Treatments = TableRegistry::get('Treatments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Treatments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
