<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TurnsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TurnsTable Test Case
 */
class TurnsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.turns',
        'app.dailyturns',
        'app.wards',
        'app.bookedexams',
        'app.exams',
        'app.users',
        'app.users_exams',
        'app.medicalrecords',
        'app.treatments',
        'app.users_medicalrecords',
        'app.users_dailyturns'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Turns') ? [] : ['className' => 'App\Model\Table\TurnsTable'];
        $this->Turns = TableRegistry::get('Turns', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Turns);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
