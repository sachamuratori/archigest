<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersMedicalrecordsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersMedicalrecordsTable Test Case
 */
class UsersMedicalrecordsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_medicalrecords',
        'app.users',
        'app.medicalrecords',
        'app.bookedexams',
        'app.exams',
        'app.wards',
        'app.dailyturns',
        'app.turns',
        'app.users_dailyturns',
        'app.rooms',
        'app.warehouses',
        'app.articles',
        'app.suppliers',
        'app.orders',
        'app.orders_articles',
        'app.warehouses_articles',
        'app.users_exams',
        'app.treatments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UsersMedicalrecords') ? [] : ['className' => 'App\Model\Table\UsersMedicalrecordsTable'];
        $this->UsersMedicalrecords = TableRegistry::get('UsersMedicalrecords', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersMedicalrecords);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
