<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersExamsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersExamsTable Test Case
 */
class UsersExamsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_exams',
        'app.users',
        'app.medicalrecords',
        'app.bookedexams',
        'app.exams',
        'app.wards',
        'app.dailyturns',
        'app.turns',
        'app.users_dailyturns',
        'app.treatments',
        'app.users_medicalrecords',
        'app.orders',
        'app.articles',
        'app.suppliers',
        'app.orders_articles',
        'app.warehouses',
        'app.warehouses_articles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UsersExams') ? [] : ['className' => 'App\Model\Table\UsersExamsTable'];
        $this->UsersExams = TableRegistry::get('UsersExams', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersExams);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
