<?php
namespace App\Test\TestCase\Controller;

use App\Controller\UsersDailyturnsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\UsersDailyturnsController Test Case
 */
class UsersDailyturnsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_dailyturns',
        'app.users',
        'app.medicalrecords',
        'app.bookedexams',
        'app.exams',
        'app.wards',
        'app.users_exams',
        'app.dailyturns',
        'app.turns',
        'app.treatments',
        'app.users_medicalrecords',
        'app.orders',
        'app.articles',
        'app.suppliers',
        'app.orders_articles',
        'app.warehouses',
        'app.warehouses_articles'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
