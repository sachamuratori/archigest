<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DailyturnsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DailyturnsController Test Case
 */
class DailyturnsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dailyturns',
        'app.wards',
        'app.turns',
        'app.bookedexams',
        'app.exams',
        'app.users',
        'app.users_dailyturns',
        'app.medicalrecords'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
