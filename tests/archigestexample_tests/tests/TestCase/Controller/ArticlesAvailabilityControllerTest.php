<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ArticlesAvailabilityController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ArticlesAvailabilityController Test Case
 */
class ArticlesAvailabilityControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.articles_availability',
        'app.articles',
        'app.suppliers',
        'app.orders',
        'app.users',
        'app.medicalrecords',
        'app.bookedexams',
        'app.exams',
        'app.wards',
        'app.dailyturns',
        'app.turns',
        'app.users_dailyturns',
        'app.rooms',
        'app.warehouses',
        'app.warehouses_articles',
        'app.users_exams',
        'app.treatments',
        'app.users_medicalrecords',
        'app.orders_articles'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
