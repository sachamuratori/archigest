<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TurnsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TurnsController Test Case
 */
class TurnsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.turns',
        'app.dailyturns',
        'app.wards',
        'app.bookedexams',
        'app.exams',
        'app.users',
        'app.users_exams',
        'app.users_medicalrecords',
        'app.medicalrecords',
        'app.treatments',
        'app.users_dailyturns'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
