Esame: Sistemi Informativi L-A
Data: 16/11/2015
Gruppo: 408

Componenti 
Numero componenti: 2
Muratori Sacha 635790 sacha.muratori@studio.unibo.it  
Fabbri Andrea 584948 andrea.fabbri42@studio.unibo.it   

Interfaccia e piattaforma: 
PhpMyAdmin e Php (CakePhp)

Titolo:
Archigest: Polyclinic Managment System

Istruzioni:
Fase 1 – Download
WAMP (WampServer) - http://www.wampserver.com/en/
Composer - https://getcomposer.org/download/

Fase 2 – Creazione nuovo progetto e Database
a) Eseguire l'applicazione wamp cliccando sull'icona. 
b) click sull'icona, una volta diventata di colore verde, che compare sulla barra inferiore a destra vicino all'orario e la data. Spostarsi su Apache->Apache modules e cliccare su "rewrite_module".
c) Assicurandosi che l'icona rimanga sempre di colore verde, spostarsi in "C:\wamp\www" e digitare questa linea:

composer create-project --prefer-dist cakephp/app *nome progetto*
(*nome progetto* = archigest)
P.S. Quando comparirà "set folder permission" premere Invio
P.P.S. Se non trova il file composer.phar scaricarlo dal sito precedente ed inserirlo in "C:\wamp\www" manualmente.

d) Andare su http://localhost/phpmyadmin/ e creare un nuovo database di nome archigest con 'collation' = 'utf8-general-ci'.
Una volta creato il database spostarsi sulla scheda 'sql' e copiare l'intero contenuto presente in 'archigest.sql' e premere 'go'. (Oppure importare il file archigest.sql su Import)

e) A questo punto svuotare nella cartella "C:\wamp\www\archigest" le cartelle "config","src" e "webroot" e copiare le stesse cartelle in "gruppo408\archigest" al suo interno

P.S. Per ogni problema o dubbio si fa riferimento alla guida di Cakephp 3.0: http://book.cakephp.org/3.0/en/installation.html

Fase 3 – Uso dell'applicazione
Spostarsi in http://localhost/archigest/ e loggarsi con:
email: sachamuratori@gmail.com
password: ciao
(le password di tutti gli account sono sempre 'ciao')	