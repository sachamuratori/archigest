<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArticlesAvailability Controller
 *
 * @property \App\Model\Table\ArticlesAvailabilityTable $ArticlesAvailability
 */
class ArticlesAvailabilityController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Articles']
        ];
        $this->set('articlesAvailability', $this->paginate($this->ArticlesAvailability));
        $this->set('_serialize', ['articlesAvailability']);
    }

    /**
     * View method
     *
     * @param string|null $id Articles Availability id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $articlesAvailability = $this->ArticlesAvailability->get($id, [
            'contain' => ['Articles']
        ]);
        $this->set('articlesAvailability', $articlesAvailability);
        $this->set('_serialize', ['articlesAvailability']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $articlesAvailability = $this->ArticlesAvailability->newEntity();
        if ($this->request->is('post')) {
            $articlesAvailability = $this->ArticlesAvailability->patchEntity($articlesAvailability, $this->request->data);
            if ($this->ArticlesAvailability->save($articlesAvailability)) {
                $this->Flash->success(__('The articles availability has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The articles availability could not be saved. Please, try again.'));
            }
        }
        $articles = $this->ArticlesAvailability->Articles->find('list', ['limit' => 200]);
        $this->set(compact('articlesAvailability', 'articles'));
        $this->set('_serialize', ['articlesAvailability']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Articles Availability id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $articlesAvailability = $this->ArticlesAvailability->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $articlesAvailability = $this->ArticlesAvailability->patchEntity($articlesAvailability, $this->request->data);
            if ($this->ArticlesAvailability->save($articlesAvailability)) {
                $this->Flash->success(__('The articles availability has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The articles availability could not be saved. Please, try again.'));
            }
        }
        $articles = $this->ArticlesAvailability->Articles->find('list', ['limit' => 200]);
        $this->set(compact('articlesAvailability', 'articles'));
        $this->set('_serialize', ['articlesAvailability']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Articles Availability id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $articlesAvailability = $this->ArticlesAvailability->get($id);
        if ($this->ArticlesAvailability->delete($articlesAvailability)) {
            $this->Flash->success(__('The articles availability has been deleted.'));
        } else {
            $this->Flash->error(__('The articles availability could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role === 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role === 1 || $role === 2){  //EMPLOYEER AND CLIENT AUTHORIZATION
            if (in_array($this->request->action, ['view', 'index', 'add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]); 
            } else {
                $this->Flash->error(__('Error in role Authorization process'));
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
}
