<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * WarehousesArticles Controller
 *
 * @property \App\Model\Table\WarehousesArticlesTable $WarehousesArticles
 */
class WarehousesArticlesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Warehouses', 'Articles']
        ];
        $this->set('warehousesArticles', $this->paginate($this->WarehousesArticles));
        $this->set('_serialize', ['warehousesArticles']);
    }

    /**
     * View method
     *
     * @param string|null $id Warehouses Article id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $warehousesArticle = $this->WarehousesArticles->get($id, [
            'contain' => ['Warehouses', 'Articles']
        ]);
        $this->set('warehousesArticle', $warehousesArticle);
        $this->set('_serialize', ['warehousesArticle']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $warehousesArticle = $this->WarehousesArticles->newEntity();
        if ($this->request->is('post')) {
            $result = $this->check_for_availability($this->request);
            if($result[0]){
                $warehousesArticle = $this->WarehousesArticles->patchEntity($warehousesArticle, $this->request->data);
                if ($this->WarehousesArticles->save($warehousesArticle)) {
                    $this->Flash->success(__('The warehouses article has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The warehouses article could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->error(__('Error. There is a current stock of '.$result[1].' articles. You can save max. '.$result[1].' articles'));
            }
        }
        $warehouses = $this->WarehousesArticles->Warehouses->find('list', ['limit' => 200]);
        $articles = $this->WarehousesArticles->Articles->find('list', ['limit' => 200]);
        $this->set(compact('warehousesArticle', 'warehouses', 'articles'));
        $this->set('_serialize', ['warehousesArticle']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Warehouses Article id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $warehousesArticle = $this->WarehousesArticles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $result = $this->check_for_availability($this->request);
            if($result[0]){
                $warehousesArticle = $this->WarehousesArticles->patchEntity($warehousesArticle, $this->request->data);
                if ($this->WarehousesArticles->save($warehousesArticle)) {
                    $this->Flash->success(__('The warehouses article has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The warehouses article could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->error(__('Error. There is a current stock of '.$result[1].' articles. You can save max. '.$result[1].' articles'));
            }
        }
        $warehouses = $this->WarehousesArticles->Warehouses->find('list', ['limit' => 200]);
        $articles = $this->WarehousesArticles->Articles->find('list', ['limit' => 200]);
        $this->set(compact('warehousesArticle', 'warehouses', 'articles'));
        $this->set('_serialize', ['warehousesArticle']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Warehouses Article id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $warehousesArticle = $this->WarehousesArticles->get($id);
        if ($this->WarehousesArticles->delete($warehousesArticle)) {
            $this->Flash->success(__('The warehouses article has been deleted.'));
        } else {
            $this->Flash->error(__('The warehouses article could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role == 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role == 1 || $role == 2){ //EMPLOYEER AND CLIENT AUTHORIZATION
            
            if (in_array($this->request->action, ['view', 'index', 'add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]); 
            } else {
                $this->Flash->error(__('Error in role Authorization process'));
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
    
    private function check_for_availability($request){
        $data = $request->data;
        $article_id = $data['article_id'];
        $warehouse_id = $data['warehouse_id'];
        
        $articlesAvailabilityTable = TableRegistry::get('ArticlesAvailability');
        $query = $articlesAvailabilityTable->find();
        $tupla = $query->select([])->where(['article_id' => $article_id])->toArray();
        
        $result = [false,$tupla[0]['availability']];
        /*debug($tupla[0]['availability']); debug($data['articlequantity']);*/
        
        if($request->params['action'] == 'edit'){
            
            $warehousesArticlesTable = TableRegistry::get('WarehousesArticles');
            $query2 = $warehousesArticlesTable->find();
            $tupla2 = $query2->select([])
                    ->where(['article_id' => $article_id])
                    ->where(['warehouse_id' => $warehouse_id])
                    ->toArray();   
            /*debug($tupla2[0]['articlequantity']);*/
            $result = [false,$tupla[0]['availability']+ $tupla2[0]['articlequantity']];
            
            if($tupla[0]['availability'] + $tupla2[0]['articlequantity'] >= $data['articlequantity']){
                $result[0] = true;
                /* cambio la quantità disponibile di quell'articolo*/
                $articlesAvailabilityTable2 = TableRegistry::get('ArticlesAvailability');
                $aa = $articlesAvailabilityTable2->get($tupla[0]['id']);
                
                $aa->availability = $tupla2[0]['articlequantity'] + $tupla[0]['availability'] - $data['articlequantity'];
                debug($aa->availability);
                
                if($articlesAvailabilityTable2->save($aa)){
                    //success
                    $result[1] = $aa->availability;
                } else {
                    $this->Flash->error(__('Error in check_for_availability. Could not save the new availability.'));
                }
            }
            
        } else { //add action
            
            if($tupla[0]['availability'] >= $data['articlequantity']){
                $result[0] = true;
                /* cambio la quantità disponibile di quell'articolo*/
                $articlesAvailabilityTable2 = TableRegistry::get('ArticlesAvailability');
                $aa = $articlesAvailabilityTable2->get($tupla[0]['id']);
                
                $aa->availability = $tupla[0]['availability'] - $data['articlequantity'];
                debug($aa->availability);
                
                if($articlesAvailabilityTable2->save($aa)){
                    //success
                    $result[1] = $aa->availability;
                } else {
                    $this->Flash->error(__('Error in check_for_availability. Could not save the new availability.'));
                }
            } 
        }
        
        return $result;
    }
    //piccolo appunto:
    /*
     * Se io ho una disponibilità di 30 articoli, ne metto 20 in un magazzino mi rimangono 10 articoli. Se quei 20 diminuiscono
     * la disponibilità non aumenta, una volta inseriti nel magazzino non si possono più riassegnare.
     * L'atto di diminuire le quantità di articoli per magazzino viene fatto dall'admin per indicare l'uso delle scorte
     * su segnalazione degli impiegati.
    */
}
