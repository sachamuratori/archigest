<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $this->set('orders', $this->paginate($this->Orders));
        $this->set('_serialize', ['orders']);
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Users', 'Articles']
        ]);
        $this->set('order', $order);
        $this->set('_serialize', ['order']);
        //$this->set('total_price', $total_price);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            /*$data = $this->add_totalprice($this->request->data);
            $this->request->data = $data;*/
            $order = $this->Orders->patchEntity($order, $this->request->data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
        }
        $users = $this->Orders->Users->find('list', ['limit' => 200])->where(['role' => 0]);
        $articles = $this->Orders->Articles->find('list', ['limit' => 200]);
        $this->set(compact('order', 'users', 'articles'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Articles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            /*$data = $this->add_totalprice($this->request->data);
            $this->request->data = $data;*/
            $order = $this->Orders->patchEntity($order, $this->request->data);
            if ($this->Orders->save($order)) {
                if($order['evaded'] == "si"){
                    $this->add_articles_availability($order);
                }
                
                $this->Flash->success(__('The order has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The order could not be saved. Please, try again.'));
            }
        }
        $users = $this->Orders->Users->find('list', ['limit' => 200])->where(['role' => 0]);
        $articles = $this->Orders->Articles->find('list', ['limit' => 200]);
        $this->set(compact('order', 'users', 'articles'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role == 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role == 1 || $role == 2){ //EMPLOYEER AND CLIENT AUTHORIZATION
            
            if (in_array($this->request->action, ['view', 'index', 'add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]); 
            } else {
                $this->Flash->error(__('Error in role Authorization process'));
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
    
    /* FUNZIONE CHE VIENE FATTA IN ORDERS ARTICLES */
    /*private function add_totalprice($data)
    {
        $totalprice = 0;
        foreach($data['articles']['_ids'] as &$article){
            $articles = TableRegistry::get('OrdersArticles');
            $query = $articles->find();
            $price = $query->select(['price'])->where(['article_id' => $article])->toArray();
            $totalprice += $price[0]->price;
        }
        
        $original = $data; $original2 = $data;
        $result1 = array_slice($original,0,2); $result2 = array_slice($original2,2,3);
        $inserted = array("totalprice" => $totalprice);
        
        $result = array_merge($result1,$inserted,$result2);
        return $result;
    }
    */
    private function add_articles_availability($order) 
    {
        /* lo posso fare solo una volta che l'ordine è stato evaso
           (evaded = "si") == merce pagata e contemporaneamente arrivata al poliambulatorio. Disponibile. */
        /*debug($order);*/
        foreach($order->articles as &$article){
            /*debug($article['id']);*/
            
            /* ricavo la quantità di quell'articolo */
            $ordersArticlesTable = TableRegistry::get('OrdersArticles');
            $query = $ordersArticlesTable->find();
            $orderarticle = $query->select(['articlequantity'])
                    ->where(['article_id' => $article['id']])
                    ->where(['order_id' => $order['id']])
                    ->toArray();
            $quantity = $orderarticle[0]['articlequantity'];
            /*debug($orderarticle);
            debug($quantity);*/
            
            /* ricavo l'id della tupla di disponibilità dell'articolo */   
            $articlesAvailabilityTable = TableRegistry::get('ArticlesAvailability');
            $query2 = $articlesAvailabilityTable->find();
            $aa_tupla = $query2->select([])->where(['article_id' => $article['id']])->toArray();
            $id = $aa_tupla[0]['id'];
            $article_availability = $aa_tupla[0]['availability'];
            /*debug($aa_tupla);
            debug($id);
            debug($article_availability);*/
            
            /* aggiorno la tupla di disponibilità dell'articolo */
            $articlesAvailabilityTable2 = TableRegistry::get('ArticlesAvailability');
            $aa = $articlesAvailabilityTable2->get($id);
            $aa->availability = $article_availability + $quantity;
            $articlesAvailabilityTable2->save($aa);
        }   
    }
    
}
