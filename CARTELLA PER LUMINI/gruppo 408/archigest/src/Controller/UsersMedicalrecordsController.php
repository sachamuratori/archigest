<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersMedicalrecords Controller
 *
 * @property \App\Model\Table\UsersMedicalrecordsTable $UsersMedicalrecords
 */
class UsersMedicalrecordsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Medicalrecords']
        ];
        $this->set('usersMedicalrecords', $this->paginate($this->UsersMedicalrecords));
        $this->set('_serialize', ['usersMedicalrecords']);
    }

    /**
     * View method
     *
     * @param string|null $id Users Medicalrecord id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersMedicalrecord = $this->UsersMedicalrecords->get($id, [
            'contain' => ['Users', 'Medicalrecords']
        ]);
        $this->set('usersMedicalrecord', $usersMedicalrecord);
        $this->set('_serialize', ['usersMedicalrecord']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersMedicalrecord = $this->UsersMedicalrecords->newEntity();
        if ($this->request->is('post')) {
            $id = $this->request->data['employeers_ids'];
            $mr_id = $this->request->data['medicalrecords_id'];
            $data = ['user_id' => $id, 'medicalrecord_id' => $mr_id];
            $this->request->data = $data;
            
            $usersMedicalrecord = $this->UsersMedicalrecords->patchEntity($usersMedicalrecord, $this->request->data);
            if ($this->UsersMedicalrecords->save($usersMedicalrecord)) {
                $this->Flash->success(__('The users medicalrecord has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users medicalrecord could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersMedicalrecords->Users->find('list', ['limit' => 200])->where(['role' => 1])->where(array('not' => array('type' => 'Janitor')));
        $medicalrecords = $this->UsersMedicalrecords->Medicalrecords->find('list', ['limit' => 200]);
        $this->set(compact('usersMedicalrecord', 'users', 'medicalrecords'));
        $this->set('_serialize', ['usersMedicalrecord']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Medicalrecord id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersMedicalrecord = $this->UsersMedicalrecords->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $id = $this->request->data['employeers_ids'];
            $mr_id = $this->request->data['medicalrecords_id'];
            $data = ['user_id' => $id, 'medicalrecord_id' => $mr_id];
            $this->request->data = $data;
            
            $usersMedicalrecord = $this->UsersMedicalrecords->patchEntity($usersMedicalrecord, $this->request->data);
            if ($this->UsersMedicalrecords->save($usersMedicalrecord)) {
                $this->Flash->success(__('The users medicalrecord has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users medicalrecord could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersMedicalrecords->Users->find('list', ['limit' => 200])->where(['role' => 1])->where(array('not' => array('type' => 'Janitor')));
        $medicalrecords = $this->UsersMedicalrecords->Medicalrecords->find('list', ['limit' => 200]);
        $this->set(compact('usersMedicalrecord', 'users', 'medicalrecords'));
        $this->set('_serialize', ['usersMedicalrecord']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Medicalrecord id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersMedicalrecord = $this->UsersMedicalrecords->get($id);
        if ($this->UsersMedicalrecords->delete($usersMedicalrecord)) {
            $this->Flash->success(__('The users medicalrecord has been deleted.'));
        } else {
            $this->Flash->error(__('The users medicalrecord could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role == 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role == 1 || $role == 2){ //EMPLOYEER AND CLIENT AUTHORIZATION
            
            if (in_array($this->request->action, ['view', 'index', 'add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]); 
            } else {
                $this->Flash->error(__('Error in role Authorization process'));
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
}
