<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Dailyturns Controller
 *
 * @property \App\Model\Table\DailyturnsTable $Dailyturns
 */
class DailyturnsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if($this->Auth->user("role") == 1){ //employeer
            
            /**********************************/
            //DATA UNA SETTIMANA
            //ORARI LE SOLITE 8 ORE 
           
            //cerca i turni di una settimana
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d', strtotime($start_date . ' +6 day'));
            $turnsTable = TableRegistry::get('Turns');
            $query = $turnsTable->find('all', array(
                    'conditions' => array(
                            "Turns.date BETWEEN '$start_date' AND '$end_date'"
                            )
                    ));
            $turns = $query->toArray();
            
            $daily_turns = [];
            $dailyturnsTable = TableRegistry::get('Dailyturns');
            foreach($turns as $turn){
                $query2 = $dailyturnsTable->find('all');
                $weekly_dailyturns = $query2
                                    ->select([])
                                    ->where(['turn_id' => $turn['id']])
                                    ->toArray();
                
                foreach($weekly_dailyturns as $weekly_dailyturn){
                    if($weekly_dailyturn != null){
                        array_push($daily_turns,$weekly_dailyturn);
                    }
                }
            } 
            //cerca i dailyturn associati a quel dottore
            $users_dailyturnsTable = TableRegistry::get('UsersDailyturns');
            $query3 = $users_dailyturnsTable->find('all');
            $users_dailyturns = $query3
                                ->select(['dailyturn_id'])
                                ->where(['user_id' => $this->Auth->user("id")])
                                ->toArray();
            
            $real_dailyturns = [];
            foreach($daily_turns as $daily_turn){
                foreach($users_dailyturns as $users_dailyturn){
                    if($daily_turn['id'] == $users_dailyturn['dailyturn_id']){
                        array_push($real_dailyturns,$daily_turn['id']);
                    }
                }
            }
            //in real_dailyturns ci sono gli id di tutti i turni di quel dottore 
            //di una settimana a partire da oggi 
            $id_first = $real_dailyturns[0];
            $id_end = end($real_dailyturns);
            
            if($id_first == $id_end){
                $query = "Dailyturns.id IN ('$real_dailyturns[0]')"; 
            } else {
                $query = "Dailyturns.id IN ('$real_dailyturns[0]',"; 
            
                foreach($real_dailyturns as $real_dailyturn){
                    if($id_first == $real_dailyturn){
                        continue;
                    }
                    if($real_dailyturn == $id_end){
                        $query = $query." '$real_dailyturn')";
                    } else {
                        $query = $query." '$real_dailyturn',";
                    }
                }
            }
            $this->set('dailyturns', $this->Paginator->paginate($this->Dailyturns, [
                                'conditions' => [
                                            $query
                                        ],
                                'limit' => 60,
                                'contain' => ['Wards', 'Turns'/*, 'Bookedexams'*/]
                            ]));
            $this->set(compact('dailyturns'));
            $this->set('_serialize', ['dailyturns']);
            
        } else { //admin
            $this->paginate = [
                'contain' => ['Wards', 'Turns'/*, 'Bookedexams'*/]
            ];
            $this->set('dailyturns', $this->paginate($this->Dailyturns));
            $this->set('_serialize', ['dailyturns']);
        }
        
        /*Choose the right index */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * View method
     *
     * @param string|null $id Dailyturn id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dailyturn = $this->Dailyturns->get($id, [
            'contain' => ['Wards', 'Turns', 'Users'/*, 'Bookedexams'*/]
        ]);
        $this->set('dailyturn', $dailyturn);
        $this->set('_serialize', ['dailyturn']);
        
        /*Choose the right view */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() //ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    {
        $dailyturn = $this->Dailyturns->newEntity();
        if ($this->request->is('post')) {
            /*debug($this->request->data);
            $i = 1;
            ini_set('max_execution_time', 300);
            while($i != 6800){*/
                $dailyturn = $this->Dailyturns->newEntity();
                $dailyturn = $this->Dailyturns->patchEntity($dailyturn, $this->request->data);
                if ($this->Dailyturns->save($dailyturn)) {
                    $this->Flash->success(__('The dailyturn has been saved.'));
                    return $this->redirect(['action' => 'index']);
                    /*$i++;*/
                    
                } else {
                    $this->Flash->error(__('The dailyturn could not be saved. Please, try again.'));
                }
            //}
        }
        $wards = $this->Dailyturns->Wards->find('list', ['limit' => 200]);
        $turns = $this->Dailyturns->Turns->find('list');
        $bookedexams = $this->Dailyturns->Bookedexams->find('list', ['limit' => 200]);
        $users = $this->Dailyturns->Users->find('list', ['limit' => 200])->where(['role' => 1]); 
       
        $this->set(compact('dailyturn', 'wards', 'turns', 'users','bookedexams'));
        $this->set('_serialize', ['dailyturn']);
        
        /*Choose the right add */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Edit method
     *
     * @param string|null $id Dailyturn id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dailyturn = $this->Dailyturns->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dailyturn = $this->Dailyturns->patchEntity($dailyturn, $this->request->data);
            if ($this->Dailyturns->save($dailyturn)) {
                $this->Flash->success(__('The dailyturn has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The dailyturn could not be saved. Please, try again.'));
            }
        }
        $wards = $this->Dailyturns->Wards->find('list', ['limit' => 200]);
        $turns = $this->Dailyturns->Turns->find('list');
        $bookedexams = $this->Dailyturns->Bookedexams->find('list', ['limit' => 200]);
        $users = $this->Dailyturns->Users->find('list', ['limit' => 200])->where(['role' => 1]); 
        
        $this->set(compact('dailyturn', 'wards', 'turns', 'users','bookedexams'));
        $this->set('_serialize', ['dailyturn']);
        
        /*Choose the right edit */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Delete method
     *
     * @param string|null $id Dailyturn id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dailyturn = $this->Dailyturns->get($id);
        if ($this->Dailyturns->delete($dailyturn)) {
            $this->Flash->success(__('The dailyturn has been deleted.'));
        } else {
            $this->Flash->error(__('The dailyturn could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role === 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role === 1){ //EMPLOYEER AUTHORIZATION 
            if (in_array($this->request->action, ['view', 'index'])) {
                 return true;
            } else if (in_array($this->request->action, ['add', 'edit', 'delete','adg'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Dailyturns', 'action' => 'index']);
            }
        } else if($role === 2){ //CLIENT AUTHORIZATION 
            if (in_array($this->request->action, ['view', 'index','add', 'edit', 'delete','adg'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]);
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }

    public function adg(){ //automatic dailyturn generator
        if ($this->request->is('post')) {
            /*debug('giorni = '.$this->request->data['day(s)']);*/
            if($this->request->data['day(s)'] <= 0 || $this->request->data['day(s)'] > 31 || $this->request->data['day(s)'] == null){
                $this->Flash->error(__('Please insert a positive number of days before submitting. Max number of days 31.'));
            } else {
                $days = $this->request->data['day(s)'];
                $start_date = date('Y-m-d');
                $end_date = date('Y-m-d', strtotime($start_date . ' +'.$days.' day'));
                
                //STEP 0: si controlla che non siano già stati creati dei turni giornalieri prima e lo si dice
                $new_date = $this->search_for_dailyturns($start_date,$end_date);
                if($new_date != null){
                    $new_start_date = date('Y-m-d', strtotime($new_date . ' + 1 day'));
                } else {
                    $new_start_date = null;
                }
                
                //STEP 1: carica tabella esami prenotati e si salva i valori degli id di quei giorni
                if($new_start_date != null){
                    $bookedexamsTable = TableRegistry::get('BookedExams');
                    $query = $bookedexamsTable->find('all', array(
                                    'conditions' => array(
                                         "Bookedexams.date BETWEEN '$new_start_date' AND '$end_date'"
                                        )
                                    ));
                    $tupla = $query->toArray();
                    $bookedexams = [];
                } else {
                    $bookedexamsTable = TableRegistry::get('BookedExams');
                    $query = $bookedexamsTable->find('all', array(
                                    'conditions' => array(
                                         "Bookedexams.date BETWEEN '$start_date' AND '$end_date'"
                                        )
                                    ));
                    $tupla = $query->toArray();
                    $bookedexams = [];
                }
                
                foreach($tupla as $bookedexam){
                    $b = ['id' => $bookedexam['id'],
                          'date' => $bookedexam['date'],
                          'time' => $bookedexam['time'],
                          'exam' => $bookedexam['exam_id']
                         ];
                    array_push($bookedexams,$b);
                }
                
                //caricare tabelle infermieri-inservienti e dottori
                $employeersTable = TableRegistry::get('Users');
                $query = $employeersTable->find('all', array(
                                'conditions' => array(
                                     "users.role = 1 AND (users.type = 'Nurse' OR users.type = 'Janitor')"
                                    )
                                ));
                $nurses_and_janitors = $query->toArray();
                $query2 = $employeersTable->find('all');
                $doctors = $query2
                                ->select([])
                                ->where(['role' => 1])
                                ->where(['type' => 'Doctor'])
                                ->toArray();
                
                /* NOTA BENE: se il tempo di running non ti basta devi aumentarlo*/
                
                //STEP 2: creazione dailyturns, inserisco data, orario, bookedexam_id se c'è e avvio algoritmo
                $hours = ["09:00","10:00","11:00","12:00","14:00","15:00","16:00","17:00"];
                if($new_start_date != null){
                    $days = $this->createDateRangeArray($new_start_date,$end_date);
                } else {
                    $days = $this->createDateRangeArray($start_date,$end_date);
                }
                
                foreach($days as $day){
                    $turn = $this->find_turn($day);
                    foreach($hours as $hour){
                        //2.a creazione turni giornalieri base per nurses e janitors
                        $this->create_dailyturns_for_nurses_and_janitors($nurses_and_janitors, $turn, $hour);
                        
                        //2.b creazione turni giornalieri per dottori
                        /*debug('giorno = '.$day.' ora = '.$hour);*/
                        $this->create_dailyturns_for_doctors($doctors, $bookedexams, $turn, $day, $hour);
                    }
                }
                return $this->redirect(['controller' => 'Dailyturns', 'action' => 'index']);
            }
        }
    }
    
    private function search_for_dailyturns($startday, $endday){
        $turnsTable = TableRegistry::get('Turns');
        $query = $turnsTable->find('all', array(
                'conditions' => array(
                        "Turns.date BETWEEN '$startday' AND '$endday'"
                        )
                ));
        $turns = $query->toArray();
        
        $dailyturns = [];
        $dailyturnsTable = TableRegistry::get('Dailyturns');
        foreach($turns as $turn){
            $query2 = $dailyturnsTable->find('all');
            $dailyturn_for_date = $query2
                                ->select(['id'])
                                ->where(['turn_id' => $turn['id']])
                                ->toArray();
            
            if($dailyturn_for_date != null){
                array_push($dailyturns,["exist" => true, "turn_id" => $turn['id']]);
            }
        }
        
        $last_element = end($dailyturns);
        if($last_element != false){
            $turns2Table = TableRegistry::get('Turns');
            $query3 = $turns2Table->find('all');
            $turn = $query3
                         ->select(['date'])
                         ->where(['id' => $last_element['turn_id']])
                         ->toArray();

            $date = $turn[0]['date'];
            $this->Flash->error(__('There are already Dailyturns until '.$date.'. The Dailyturns after that date have been created.'));     
            return $date;
        } else {
            return null;
        }
    }
    
    private function create_dailyturns_for_doctors($doctors, $bookedexams, $turn, $day, $hour){
        
        //2.b.a guardare se ci sono esami prenotati (anche più di uno) per giorno/ora
        $results = $this->bookedexam_and_ward($bookedexams, $day, $hour);
        
        $not_available_doctors = []; //per evitare che lo stesso dottore venga scelto due volte nello stesso orario e stesso giorno 
        
        //2.b.b
        if($results != null){ //se entra dentro ci sono gli esami prenotati per giorno/ora
            //caricare la tabella degli users_exams per vedere i dottori che possono eseguire quell'esame
            foreach($results as $result){
                //debug($result);
                $users_examsTable = TableRegistry::get('UsersExams');
                $query = $users_examsTable->find('all');
                $enable_docs = $query
                            ->select(['user_id'])
                            ->where(['exam_id' => $result['exam_id']])
                            ->toArray();
                
                //debug($enable_docs);
                //controlla che il dottore scelto casualmente non sia già stato usato altrimenti rifà il ciclo
                $stay = 1; $a = 1;
                while($stay == 1){
                    //debug('dentro il while. giorno = '.$day.' ora = '.$hour);
                    $rand_key = array_rand($enable_docs);   
                    //debug('random key = '.$rand_key);
                    //guarda tutta la lista di dottori che sono già stati scelti e se l'id random corrisponde a uno di questi
                    //ricomincia il ciclo
                    foreach($not_available_doctors as $not_available_doctor){
                        if($enable_docs[$rand_key]['user_id'] == $not_available_doctor['id']){
                            //debug('id utente random corrisponde a uno dei dottori not available');
                            $a = 0;
                        }
                    }
                    //debug('a = '.$a);
                    if($a == 1){
                        $stay = 0;
                    }
                    //debug('stay = '.$stay);
                }
                array_push($not_available_doctors,["id" => $enable_docs[$rand_key]['user_id']]);
                /*debug('not available doctors');
                debug($not_available_doctors);*/
                
                //scrivi tupla
                $data = [  
                        "ward_id" => $result['ward_id'],
                        "turn_id" => $turn,
                        "bookedexam_id" => $result['bookedexam_id'],
                        "time" => $hour,
                ];
                $dailyturn = $this->Dailyturns->newEntity();
                $dailyturn = $this->Dailyturns->patchEntity($dailyturn, $data);
                
                if ($this->Dailyturns->save($dailyturn)) {
                    $dailyturn_id = $dailyturn['id']; 
                    $user_id = $enable_docs[$rand_key]['user_id'];
                    
                    $user_dailyturnTable = TableRegistry::get('UsersDailyturns');
                    $query = $user_dailyturnTable->query();
                    $query->insert(['user_id','dailyturn_id'])->values(['user_id' => $user_id, 'dailyturn_id' => $dailyturn_id])->execute();
                } else {
                    $this->Flash->error(__('The Dailyturn for Doctor with ward and bookedexam could not be saved. Please Delete all the dailyturns you have created and retry'));
                } 
            }
        }
        
        $data = [  
                  "ward_id" => null,
                  "turn_id" => $turn,
                  "bookedexam_id" => null,
                  "time" => $hour,
        ];
        
        foreach($doctors as $doctor){ //doctors
            $already_saved = false;
            foreach($not_available_doctors as $not_available_doctor){
                if($doctor['id'] == $not_available_doctor['id']){
                    $already_saved = true;
                }
            }
            if(!$already_saved){
                //salva dailyturn
                $dailyturn = $this->Dailyturns->newEntity();
                $dailyturn = $this->Dailyturns->patchEntity($dailyturn, $data);
                if ($this->Dailyturns->save($dailyturn)) {
                    //creazione di users_dailyturns;
                    $dailyturn_id = $dailyturn['id']; 
                    $id = $doctor['id'];

                    $user_dailyturnTable = TableRegistry::get('UsersDailyturns');
                    $query = $user_dailyturnTable->query();
                    $query->insert(['user_id','dailyturn_id'])->values(['user_id' => $id, 'dailyturn_id' => $dailyturn_id])->execute();
                } else {
                    $this->Flash->error(__('The Dailyturn for Doctor could not be saved. Please Delete all the dailyturns you have created and retry'));
                } 
            } else {
                //dottore già salvato non fai nulla
            }
        }
    }
    
    private function bookedexam_and_ward($bookedexams, $day, $hour){
        $results = []; 
        foreach($bookedexams as $bookedexam){
            //controlla per ogni esame prenotato che corrisponda alla data e all'ora
            if($bookedexam['date']->format("Y-m-d") == $day && $bookedexam['time']->format("H:i") == $hour){ 
                $WardsTable = TableRegistry::get('Exams');
                $query = $WardsTable->find('all');
                $ward = $query
                                ->select(['ward_id'])
                                ->where(['id' => $bookedexam['exam']])
                                ->toArray();
                
                array_push($results,["bookedexam_id" => $bookedexam['id'],"ward_id" => $ward[0]['ward_id'],"exam_id" =>$bookedexam['exam']]);
            }   
        }
        return $results;
    }
    
    private function create_dailyturns_for_nurses_and_janitors($employeers, $turn, $hour){          
        $data = [  
                "ward_id" => null,
                "turn_id" => $turn,
                "bookedexam_id" => null,
                "time" => $hour,
            ];
        
        $dailyturn = $this->Dailyturns->newEntity();
        $dailyturn = $this->Dailyturns->patchEntity($dailyturn, $data);
        
        if ($this->Dailyturns->save($dailyturn)) {
            foreach($employeers as $employeer){ //nurses and janitors
                //creazione di users_dailyturns;
                $dailyturn_id = $dailyturn['id']; 
                $id = $employeer['id'];
                
                $user_dailyturnTable = TableRegistry::get('UsersDailyturns');
                $query = $user_dailyturnTable->query();
                $query->insert(['user_id','dailyturn_id'])->values(['user_id' => $id, 'dailyturn_id' => $dailyturn_id])->execute();
            }
        } else {
            $this->Flash->error(__('The Dailyturn for Nurses and Janitors could not be saved. Please Delete all the dailyturns you have created and retry'));
        } 
    }
    
    private function find_turn($day){
        $TurnsTable = TableRegistry::get('Turns');
        $query = $TurnsTable->find('all');
        $turn = $query
                ->select(['id'])
                ->where(['date' => $day])
                ->toArray();
        return $turn[0]['id'];
    }
    
    /* do not touch */
    private function createDateRangeArray($strDateFrom,$strDateTo){
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.

        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange=array();

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange,date('Y-m-d',$iDateFrom));
            }
        }
        return $aryRange;
    }
            
}
