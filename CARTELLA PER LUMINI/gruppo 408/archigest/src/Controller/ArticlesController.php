<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;


/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class ArticlesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Suppliers']
        ];
        $this->set('articles', $this->paginate($this->Articles));
        $this->set('_serialize', ['articles']);
        
        /*Choose the right index */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Suppliers', 'Orders', 'Warehouses']
        ]);
        $this->set('article', $article);
        $this->set('_serialize', ['article']);
        
        /*Choose the right view */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                /* */
                $this->add_article_availability($article['id']);
                /* */
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
        }
        $suppliers = $this->Articles->Suppliers->find('list', ['limit' => 200]);
        $orders = $this->Articles->Orders->find('list', ['limit' => 200]);
        $warehouses = $this->Articles->Warehouses->find('list', ['limit' => 200]);
        $this->set(compact('article', 'suppliers', 'orders', 'warehouses'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Orders', 'Warehouses']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
        }
        $suppliers = $this->Articles->Suppliers->find('list', ['limit' => 200]);
        $orders = $this->Articles->Orders->find('list', ['limit' => 200]);
        $warehouses = $this->Articles->Warehouses->find('list', ['limit' => 200]);
        $this->set(compact('article', 'suppliers', 'orders', 'warehouses'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The article has been deleted.'));
        } else {
            $this->Flash->error(__('The article could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        $type =  $this->Auth->user("type");
        if($role === 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role === 1 || $type != 'Janitor'){ //EMPLOYEER AUTHORIZATION NO JANITOR
            if (in_array($this->request->action, ['view', 'index'])) {
                 return true;
            } else if (in_array($this->request->action, ['add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Articles', 'action' => 'index']);
            }
        } else if($role === 1 || $type == 'Janitor'){ //JANITOR AUTHORIZATION 
             if (in_array($this->request->action, ['view'])) {
                 return true;
            } else if (in_array($this->request->action, ['index','add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Articles', 'action' => 'index']);
            }
        } else if($role === 2){  //CLIENT AUTHORIZATION
            if (in_array($this->request->action, ['view', 'index', 'add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]); 
            } else {
                $this->Flash->error(__('Error in role Authorization process'));
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
    
    private function add_article_availability($article_id){
        $articlesAvailabilityTable = TableRegistry::get('ArticlesAvailability');
        $query = $articlesAvailabilityTable->query();
        $query->insert(['article_id'])->values(['article_id' => $article_id])->execute();
    }
}
