<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Treatments Controller
 *
 * @property \App\Model\Table\TreatmentsTable $Treatments
 */
class TreatmentsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if($this->Auth->user("role") == 1){ //employeer doctor o nurse
            //inpagina solo i trattamenti delle cc che hanno un riferimento in users_medicalrecords
            //cerca i dailyturn associati a quel dottore
            $users_medicalrecordsTable = TableRegistry::get('UsersMedicalrecords');
            $querysql = $users_medicalrecordsTable->find('all');
            $users_medicalrecords = $querysql
                                ->select(['medicalrecord_id'])
                                ->where(['user_id' => $this->Auth->user("id")])
                                ->toArray();
            
            $medicalrecords = [];
            foreach($users_medicalrecords as $users_medicalrecord){
                array_push($medicalrecords,$users_medicalrecord['medicalrecord_id']);
            }
            //in real_dailyturns ci sono gli id di tutti i turni di quel dottore 
            //di una settimana a partire da oggi
            if($medicalrecords != null){
                $id_first = $medicalrecords[0];
                $id_end = end($medicalrecords);

                if($id_first == $id_end){
                    $query = "Treatments.medicalrecord_id IN ('$medicalrecords[0]')"; 
                } else {
                    $query = "Treatments.medicalrecord_id IN ('$medicalrecords[0]',"; 

                    foreach($medicalrecords as $medicalrecord){
                        if($id_first == $medicalrecord){
                            continue;
                        }
                        if($medicalrecord == $id_end){
                            $query = $query." '$medicalrecord')";
                        } else {
                            $query = $query." '$medicalrecord',";
                        }
                    }
                }
                $this->set('treatments', $this->Paginator->paginate($this->Treatments, [
                                    'conditions' => [
                                                $query
                                            ],
                                    'limit' => 100,
                                    'contain' => ['Medicalrecords', 'Users']
                ]));
                $this->set(compact('treatments'));
                $this->set('_serialize', ['treatments']);
                
            } else {
                $query = "Treatments.medicalrecord_id IN (null)"; 
                $this->set('treatments', $this->Paginator->paginate($this->Treatments, [
                                    'conditions' => [
                                                $query
                                            ],
                                    'limit' => 100,
                                    'contain' => ['Medicalrecords', 'Users']
                ]));
                $this->set(compact('treatments'));
                $this->set('_serialize', ['treatments']);
            }
            
        } else {
            $this->paginate = [
                'contain' => ['Medicalrecords', 'Users']
            ];
            $this->set('treatments', $this->paginate($this->Treatments));
            $this->set('_serialize', ['treatments']);
        }
        
        /*Choose the right index */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * View method
     *
     * @param string|null $id Treatment id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $treatment = $this->Treatments->get($id, [
            'contain' => ['Medicalrecords', 'Users']
        ]);
        $this->set('treatment', $treatment);
        $this->set('_serialize', ['treatment']);
        $this->set('medicalrecord_id', $this->Auth->user("medicalrecord_id"));
        
        /*Choose the right view */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $treatment = $this->Treatments->newEntity();
        if ($this->request->is('post')) {
            if($this->Auth->user('role') == 1){
                /****************/
                $usersmedicalrecordsTable = TableRegistry::get('UsersMedicalrecords');
                $query = $usersmedicalrecordsTable->find('all');
                $ids = $query
                        ->select(['medicalrecord_id'])
                        ->where(['user_id' => $this->Auth->user("id")])
                        ->toArray();
                $save = false;
                foreach($ids as &$id){
                    if($this->request->data['medicalrecord_id'] == $id['medicalrecord_id']){
                        $save = true;
                    }
                }
                if($save == false){
                    $this->Flash->error(__('The treatment could not be saved. This is not one of your medicalrecord.'));
                } else {
                    $treatment = $this->Treatments->patchEntity($treatment, $this->request->data);
                    if ($this->Treatments->save($treatment)) {
                        $this->Flash->success(__('The treatment has been saved.'));
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('The treatment could not be saved. Please, try again.'));
                    }
                }
                
            } else { //admin
                $treatment = $this->Treatments->patchEntity($treatment, $this->request->data);
                if ($this->Treatments->save($treatment)) {
                    $this->Flash->success(__('The treatment has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The treatment could not be saved. Please, try again.'));
                }
            }
        }
        
        $medicalrecords = $this->Treatments->Medicalrecords->find('list', ['limit' => 200]);
        if($this->Auth->user('role') == 1){ //è un dottore perchè janitor e nurse non possono accedervi
            $users = $this->Treatments->Users->find('list')->where(['id' => $this->Auth->user("id")]);
        } else {
            $users = $this->Treatments->Users->find('list', ['limit' => 200])->where(['type' => "Doctor"]); 
        }
        $this->set(compact('treatment', 'medicalrecords', 'users'));
        $this->set('_serialize', ['treatment']);
        
        /*Choose the right add */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Edit method
     *
     * @param string|null $id Treatment id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $treatment = $this->Treatments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if($this->Auth->user('role') == 1){
                /****************/
                $usersmedicalrecordsTable = TableRegistry::get('UsersMedicalrecords');
                $query = $usersmedicalrecordsTable->find('all');
                $ids = $query
                        ->select(['medicalrecord_id'])
                        ->where(['user_id' => $this->Auth->user("id")])
                        ->toArray();
                $save = false;
                foreach($ids as &$id){
                    if($this->request->data['medicalrecord_id'] == $id['medicalrecord_id']){
                        $save = true;
                    }
                }
                if($save == false){
                    $this->Flash->error(__('You cannot edit this Treatment. This is not one of your medicalrecord.'));
                } else {
                    $treatment = $this->Treatments->patchEntity($treatment, $this->request->data);
                    if ($this->Treatments->save($treatment)) {
                        $this->Flash->success(__('The treatment has been saved.'));
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('The treatment could not be saved. Please, try again.'));
                    }
                }
                
            } else { //admin
                $treatment = $this->Treatments->patchEntity($treatment, $this->request->data);
                if ($this->Treatments->save($treatment)) {
                    $this->Flash->success(__('The treatment has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->Flash->error(__('The treatment could not be saved. Please, try again.'));
                }
            }
        }
        
        $medicalrecords = $this->Treatments->Medicalrecords->find('list', ['limit' => 200]);
        if($this->Auth->user('role') == 1){ //è un dottore perchè janitor e nurse non possono accedervi
            $users = $this->Treatments->Users->find('list')->where(['id' => $this->Auth->user('id')]);
        } else {
            $users = $this->Treatments->Users->find('list', ['limit' => 200])->where(['type' => "Doctor"]); 
        }
        $this->set(compact('treatment', 'medicalrecords', 'users'));
        $this->set('_serialize', ['treatment']);
        
        /*Choose the right edit */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Delete method
     *
     * @param string|null $id Treatment id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $treatment = $this->Treatments->get($id);
        if ($this->Treatments->delete($treatment)) {
            $this->Flash->success(__('The treatment has been deleted.'));
        } else {
            $this->Flash->error(__('The treatment could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        $type =  $this->Auth->user("type");
        if($role == 0){
            return parent::isAuthorized($user);
            
        } else if(($role == 1) && (strcmp($type,'Doctor') == 0)){ //EMPLOYEER DOCTOR AUTHORIZATION  
            if (in_array($this->request->action, ['index','view','add','edit','delete'])) {
                 return true; 
            }
        } else if(($role == 1) && (strcmp($type,'Nurse') == 0)){ //EMPLOYEER NURSE AUTHORIZATION  
            if (in_array($this->request->action, ['index','view'])) {
                 return true; 
            } else if (in_array($this->request->action, ['add','edit','delete'])) {
                    $this->Flash->error(__('You cannot access that location'));
                    $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]);
            }
        } else if(($role == 1) && (strcmp($type,'Janitor') == 0)){ //EMPLOYEER JANITOR AUTHORIZATION  
            if (in_array($this->request->action, ['index','view','add','edit','delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]);
            }
        } else if($role == 2){ //CLIENT AUTHORIZATION
            if (in_array($this->request->action, ['index','add','edit','delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]);
                
            } else if (in_array($this->request->action, ['view'])) {
                $treatment_id = $this->request->params['pass'][0];
                $treatments = TableRegistry::get('Treatments');
                $query = $treatments->find('all');
                $id = $query
                             ->select(['medicalrecord_id'])
                             ->where(['id' => $treatment_id])
                             ->toArray();
                if ($id[0]['medicalrecord_id'] == $user['medicalrecord_id']) {
                    return true;
                } else {
                    $this->Flash->error(__('You cannot access that location'));
                    $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]);
                }
            }
            
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
}
