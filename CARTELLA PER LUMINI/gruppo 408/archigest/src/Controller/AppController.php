<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'], 
            'authenticate' => [
                'Form' => [
                    'fields' => 
                        [
                            'username' => 'email',
                            'password' => 'password'
                        ]
               ]
            ],
            'loginAction' => [
			'controller' => 'Users',
			'action' => 'login'
	    ],
            'logoutRedirect' => [
                'controller' => 'Pages',
                'action' => 'display',
                'home'
            ]
        ]);
    }
    public function isAuthorized($user)
    {
        // Admin can access every action
        if (isset($user['role']) && $user['role'] === 0) {
            return true;
        }

        // Default deny
        return false;
    }
    
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
        
        $this->layoutVariables();        
    }
    
    private function layoutVariables(){
        $this->set('userId', $this->Auth->user("id"));
        if($this->Auth->user("role") == 0){
            $this->set('userRole', "Admin");
        } else if($this->Auth->user("role") == 1) {
            if($this->Auth->user("type") == "Doctor"){
                $this->set('userRole', "Doctor");
            } else if($this->Auth->user("type") == "Nurse"){
                $this->set('userRole', "Nurse");
            } else if($this->Auth->user("type") == "Janitor"){
                $this->set('userRole', "Janitor");
            } else {
                $this->Flash->error(__('Error in layoutVariables in class AppController, Employeer section'));
            }
        } else if($this->Auth->user("role") == 2) {
            $this->set('userRole', "Client");
        } else {
            $this->Flash->error(__('Error in layoutVariables in class AppController'));
        }
        $this->set('userName', $this->Auth->user("name"));
        $this->set('userSurname', $this->Auth->user("surname"));
        $this->set('userAction', $this->request->params['action']);
    }
    
    /* Funzioni per il routing basato sul ruolo dell'user*/
    /******************************** ROUTING ****************************************/
    protected function ProcessView($methodName){
        $role =  $this->Auth->user("role");
                //$this->viewVars['user']['role'];
        
        if($role == 0){
            $this->render($methodName."_admin");
        } else if($role == 1){
            $this->ProcessViewForEmployeer($methodName);
        } else if($role == 2){
            $this->render($methodName); //template senza suffisso sono del client
        } else{
            $this->Flash->error(__('Error in Process View'));
        }       
    }
        
    protected function ProcessViewForEmployeer($methodName){
        $type = $this->Auth->user("type");
                //$this->viewVars['user']['type']; 
        //cambia le view in base a se sei admin, client, doctor, nurse, janitor
                
        if(strcmp($type,'Doctor') == 0 || strcmp($type,'doctor') == 0){
            $this->render($methodName."_employeer_doctor");
        } else if(strcmp($type,'Nurse') == 0 || strcmp($type,'nurse') == 0  ){
            $this->render($methodName."_employeer_nurse");
        } else if(strcmp($type,'Janitor') == 0 || strcmp($type,'janitor') == 0 ){
            $this->render($methodName."_employeer_janitor"); 
        } else{
            $this->Flash->error(__('Error in Process View For Employeer'));
        }       
    }
}
