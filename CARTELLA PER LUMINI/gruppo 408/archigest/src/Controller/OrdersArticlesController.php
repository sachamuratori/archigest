<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * OrdersArticles Controller
 *
 * @property \App\Model\Table\OrdersArticlesTable $OrdersArticles
 */
class OrdersArticlesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Orders', 'Articles']
        ];
        $this->set('ordersArticles', $this->paginate($this->OrdersArticles));
        $this->set('_serialize', ['ordersArticles']);
    }

    /**
     * View method
     *
     * @param string|null $id Orders Article id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ordersArticle = $this->OrdersArticles->get($id, [
            'contain' => ['Orders', 'Articles']
        ]);
        $this->set('ordersArticle', $ordersArticle);
        $this->set('_serialize', ['ordersArticle']);/*
        debug($ordersArticle['article']['price']);
        die;*/
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ordersArticle = $this->OrdersArticles->newEntity();
        if ($this->request->is('post')) {
            $price = $this->calculate_price($this->request->data);
            $this->request->data['price'] = $price;
            $this->add_totalprice_to_order($this->request->data);
            $ordersArticle = $this->OrdersArticles->patchEntity($ordersArticle, $this->request->data);
            if ($this->OrdersArticles->save($ordersArticle)) {
                $this->Flash->success(__('The orders article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The orders article could not be saved. Please, try again.'));
            }
        }
        $orders = $this->OrdersArticles->Orders->find('list', ['limit' => 200])->where(['evaded' => 'no']);
        $articles = $this->OrdersArticles->Articles->find('list', ['limit' => 200]);
        $this->set(compact('ordersArticle', 'orders', 'articles'));
        $this->set('_serialize', ['ordersArticle']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Orders Article id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ordersArticle = $this->OrdersArticles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $price = $this->calculate_price($this->request->data);
            $this->request->data['price'] = $price;
            $this->add_totalprice_to_order($this->request->data);
            $ordersArticle = $this->OrdersArticles->patchEntity($ordersArticle, $this->request->data);
            if ($this->OrdersArticles->save($ordersArticle)) {
                $this->Flash->success(__('The orders article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The orders article could not be saved. Please, try again.'));
            }
        }
        $orders = $this->OrdersArticles->Orders->find('list', ['limit' => 200])->where(['evaded' => 'no']);
        $articles = $this->OrdersArticles->Articles->find('list', ['limit' => 200]);
        $this->set(compact('ordersArticle', 'orders', 'articles'));
        $this->set('_serialize', ['ordersArticle']);
    }

    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role == 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role == 1 || $role == 2){ //EMPLOYEER AND CLIENT AUTHORIZATION
            
            if (in_array($this->request->action, ['view', 'index', 'add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]); 
            } else {
                $this->Flash->error(__('Error in role Authorization process'));
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
    
    private function calculate_price($data){
        $quantity = $data['articlequantity'];
        $id = $data['article_id'];
        $articles = TableRegistry::get('Articles');
        $query = $articles->find('all');
        $article = $query
                         ->select(['price'])
                         ->where(['id' => $id])
                         ->first();
        $price = $quantity * $article['price'];
        return $price;
    }
    
    private function add_totalprice_to_order($data)
    {
        /*debug($data['article_id']);
        debug($data['order_id']);
        debug($data['price']);*/
        
        $id = $data['order_id'];
        
        $ordersarticles = TableRegistry::get('OrdersArticles');
        $query = $ordersarticles->find();
        $orderarticle = $query->select([])->where(['order_id' => $data['order_id']])->where(['article_id' => $data['article_id']])->toArray();
        
        $old_price = $orderarticle[0]['price'];
        $new_price = $data['price'];
        
        $orders = TableRegistry::get('Orders');
        $query = $orders->find();
        $order = $query->select(['totalprice'])->where(['id' => $data['order_id']])->first();
        
        $totalprice = $order['totalprice'] - $old_price + $new_price;

        $ordersTable = TableRegistry::get('Orders');
        $order2 = $ordersTable->get($id); 
       
        $order2->totalprice = $totalprice;
        $ordersTable->save($order2); //orders?
    }
    /**
     * Delete method
     *
     * @param string|null $id Orders Article id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ordersArticle = $this->OrdersArticles->get($id);
        if ($this->OrdersArticles->delete($ordersArticle)) {
            $this->Flash->success(__('The orders article has been deleted.'));
        } else {
            $this->Flash->error(__('The orders article could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
