<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Medicalrecords
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Bookedexams
 * @property \Cake\ORM\Association\HasMany $Orders
 * @property \Cake\ORM\Association\HasMany $Treatments
 * @property \Cake\ORM\Association\HasMany $Users
 * @property \Cake\ORM\Association\BelongsToMany $Dailyturns
 * @property \Cake\ORM\Association\BelongsToMany $Exams
 * @property \Cake\ORM\Association\BelongsToMany $Medicalrecords
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Medicalrecords', [
            'foreignKey' => 'medicalrecord_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Bookedexams', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Treatments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsToMany('Dailyturns', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'dailyturn_id',
            'joinTable' => 'users_dailyturns'
        ]);
        $this->belongsToMany('Exams', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'exam_id',
            'joinTable' => 'users_exams'
        ]);
        $this->belongsToMany('Medicalrecords', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'medicalrecord_id',
            'joinTable' => 'users_medicalrecords'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('role', 'valid', ['rule' => 'numeric'])
            ->requirePresence('role', 'create')
            ->notEmpty('role');

        $validator
            ->add('email', 'valid', ['rule' => 'email'])
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password')
            ->add('password', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('surname', 'create')
            ->notEmpty('surname');

        $validator
            ->requirePresence('sex', 'create')
            ->notEmpty('sex');

        $validator
            ->add('dateofbirth', 'valid', ['rule' => 'date'])
            ->requirePresence('dateofbirth', 'create')
            ->notEmpty('dateofbirth');

        $validator
            ->requirePresence('cf', 'create')
            ->notEmpty('cf')
            ->add('cf', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->requirePresence('postalcode', 'create')
            ->notEmpty('postalcode');

        $validator
            ->requirePresence('province', 'create')
            ->notEmpty('province');

        $validator
            ->requirePresence('country', 'create')
            ->notEmpty('country');

        $validator
            ->requirePresence('telephone1', 'create')
            ->notEmpty('telephone1');

        $validator
            ->allowEmpty('telephone2');

        $validator
            ->allowEmpty('ct');

        $validator
            ->allowEmpty('type');

        $validator
            ->add('salary', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('salary');

        $validator
            ->allowEmpty('specialization');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['medicalrecord_id'], 'Medicalrecords'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
