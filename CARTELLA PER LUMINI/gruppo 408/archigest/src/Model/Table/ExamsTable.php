<?php
namespace App\Model\Table;

use App\Model\Entity\Exam;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Exams Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Wards
 * @property \Cake\ORM\Association\HasMany $Bookedexams
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class ExamsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('exams');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Wards', [
            'foreignKey' => 'ward_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Bookedexams', [
            'foreignKey' => 'exam_id'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'exam_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'users_exams'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ward_id'], 'Wards'));
        return $rules;
    }
}
