<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity.
 *
 * @property int $id
 * @property int $role
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $surname
 * @property string $sex
 * @property \Cake\I18n\Time $dateofbirth
 * @property string $cf
 * @property string $address
 * @property string $city
 * @property string $postalcode
 * @property string $province
 * @property string $country
 * @property string $telephone1
 * @property string $telephone2
 * @property int $medicalrecord_id
 * @property string $ct
 * @property string $type
 * @property int $salary
 * @property string $specialization
 * @property int $user_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Medicalrecord[] $medicalrecords
 * @property \App\Model\Entity\User[] $users
 * @property \App\Model\Entity\Bookedexam[] $bookedexams
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Treatment[] $treatments
 * @property \App\Model\Entity\Dailyturn[] $dailyturns
 * @property \App\Model\Entity\Exam[] $exams
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
    
    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }
}
