<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Medicalrecord Entity.
 *
 * @property int $id
 * @property int $client_id
 * @property string $client_name
 * @property string $client_surname
 * @property string $client_ct
 * @property string $name
 * @property string $anamnesis
 * @property string $medicalhistory
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Bookedexam[] $bookedexams
 * @property \App\Model\Entity\Treatment[] $treatments
 * @property \App\Model\Entity\User[] $users
 */
class Medicalrecord extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
