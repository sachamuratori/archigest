<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bookedexam Entity.
 *
 * @property int $id
 * @property int $exam_id
 * @property \App\Model\Entity\Exam $exam
 * @property \Cake\I18n\Time $date
 * @property \Cake\I18n\Time $time
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $medicalrecord_id
 * @property \App\Model\Entity\Medicalrecord $medicalrecord
 * @property string $result
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Dailyturn[] $dailyturns
 */
class Bookedexam extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
