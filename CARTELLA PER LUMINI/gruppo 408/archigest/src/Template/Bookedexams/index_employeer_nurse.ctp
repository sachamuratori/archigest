<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Nurse') ?></li>
        <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="bookedexams index large-9 medium-8 columns content">
    <h3><?= __('Bookedexams') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('exam_id') ?></th>
                <th><?= $this->Paginator->sort('date') ?></th>
                <th><?= $this->Paginator->sort('time') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bookedexams as $bookedexam): ?>
            <tr>
                <td><?= $this->Number->format($bookedexam->id) ?></td>
                <td><?= $bookedexam->has('exam') ? $this->Html->link($bookedexam->exam->name, ['controller' => 'Exams', 'action' => 'view', $bookedexam->exam->id]) : '' ?></td>
                <td><?= h($bookedexam->date->format('Y-m-d')) ?></td>
                <td><?= h($bookedexam->time->format('H:i')) ?></td>
                <td><?= $bookedexam->has('user') ? $this->Html->link($bookedexam->user->name.' '.$bookedexam->user->surname, ['controller' => 'Users', 'action' => 'view', $bookedexam->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $bookedexam->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
