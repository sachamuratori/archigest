<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Janitor') ?></li>
        <li><?= $this->Html->link(__('List Bookedexams'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="bookedexams view large-9 medium-8 columns content">
    <h3><?= h($bookedexam->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Exam') ?></th>
            <td><?= $bookedexam->has('exam') ? $this->Html->link($bookedexam->exam->name, ['controller' => 'Exams', 'action' => 'view', $bookedexam->exam->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $bookedexam->has('user') ? $this->Html->link($bookedexam->user->name.' '.$bookedexam->user->surname, ['controller' => 'Users', 'action' => 'view', $bookedexam->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Result') ?></th>
            <td><?= h($bookedexam->result) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($bookedexam->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date') ?></th>
            <td><?= h($bookedexam->date->format('Y-m-d')) ?></tr>
        </tr>
        <tr>
            <th><?= __('Time') ?></th>
            <td><?= h($bookedexam->time->format('H:i')) ?></tr>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($bookedexam->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($bookedexam->modified) ?></tr>
        </tr>
    </table>
</div>
