<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Nurse') ?></li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="treatments index large-9 medium-8 columns content">
    <h3><?= __('Treatments') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('medicalrecord_id') ?></th>
                <th><?= $this->Paginator->sort('doctor') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th><?= $this->Paginator->sort('startdate') ?></th>
                <th><?= $this->Paginator->sort('enddate') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($treatments as $treatment): ?>
            <tr>
                <td><?= $this->Number->format($treatment->id) ?></td>
                <td><?= $treatment->has('medicalrecord') ? $this->Html->link($treatment->medicalrecord->name, ['controller' => 'Medicalrecords', 'action' => 'view', $treatment->medicalrecord->id]) : '' ?></td>
                <td><?= $treatment->has('user') ? $this->Html->link($treatment->user->name.' '.$treatment->user->surname, ['controller' => 'Users', 'action' => 'view', $treatment->user->id]) : '' ?></td>
                <td><?= h($treatment->name) ?></td>
                <td><?= h($treatment->description) ?></td>
                <td><?= h($treatment->startdate) ?></td>
                <td><?= h($treatment->enddate) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $treatment->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
