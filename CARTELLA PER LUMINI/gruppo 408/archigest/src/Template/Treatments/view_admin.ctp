<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('Edit Treatment'), ['action' => 'edit', $treatment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Treatment'), ['action' => 'delete', $treatment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $treatment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Treatments'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Treatment'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Medicalrecord'), ['controller' => 'Medicalrecords', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="treatments view large-9 medium-8 columns content">
    <h3><?= h($treatment->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Medicalrecord') ?></th>
            <td><?= $treatment->has('medicalrecord') ? $this->Html->link($treatment->medicalrecord->name, ['controller' => 'Medicalrecords', 'action' => 'view', $treatment->medicalrecord->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $treatment->has('user') ? $this->Html->link($treatment->user->name, ['controller' => 'Users', 'action' => 'view', $treatment->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($treatment->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= h($treatment->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($treatment->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Startdate') ?></th>
            <td><?= h($treatment->startdate) ?></tr>
        </tr>
        <tr>
            <th><?= __('Enddate') ?></th>
            <td><?= h($treatment->enddate) ?></tr>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($treatment->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($treatment->modified) ?></tr>
        </tr>
    </table>
</div>
