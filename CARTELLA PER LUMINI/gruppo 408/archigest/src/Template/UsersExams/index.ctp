<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('New Users Exam'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Exam'), ['controller' => 'Exams', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersExams index large-9 medium-8 columns content">
    <h3><?= __('Users Exams') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('exam_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersExams as $usersExam): ?>
            <tr>
                <td><?= $this->Number->format($usersExam->id) ?></td>
                <td><?= $usersExam->has('user') ? $this->Html->link($usersExam->user->name, ['controller' => 'Users', 'action' => 'view', $usersExam->user->id]) : '' ?></td>
                <td><?= $usersExam->has('exam') ? $this->Html->link($usersExam->exam->name, ['controller' => 'Exams', 'action' => 'view', $usersExam->exam->id]) : '' ?></td>
                <td><?= h($usersExam->created) ?></td>
                <td><?= h($usersExam->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usersExam->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usersExam->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usersExam->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersExam->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
