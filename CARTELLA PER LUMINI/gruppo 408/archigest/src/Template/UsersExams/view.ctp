<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('Edit Users Exam'), ['action' => 'edit', $usersExam->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Exam'), ['action' => 'delete', $usersExam->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersExam->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Exams'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Exam'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Exam'), ['controller' => 'Exams', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersExams view large-9 medium-8 columns content">
    <h3><?= h($usersExam->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $usersExam->has('user') ? $this->Html->link($usersExam->user->name, ['controller' => 'Users', 'action' => 'view', $usersExam->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Exam') ?></th>
            <td><?= $usersExam->has('exam') ? $this->Html->link($usersExam->exam->name, ['controller' => 'Exams', 'action' => 'view', $usersExam->exam->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($usersExam->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($usersExam->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($usersExam->modified) ?></tr>
        </tr>
    </table>
</div>
