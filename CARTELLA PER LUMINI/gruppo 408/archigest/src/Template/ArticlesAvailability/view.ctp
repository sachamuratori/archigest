<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('Edit Articles Availability'), ['action' => 'edit', $articlesAvailability->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Articles Availability'), ['action' => 'delete', $articlesAvailability->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articlesAvailability->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Articles Availability'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Articles Availability'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="articlesAvailability view large-9 medium-8 columns content">
    <h3><?= h($articlesAvailability->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Article') ?></th>
            <td><?= $articlesAvailability->has('article') ? $this->Html->link($articlesAvailability->article->name, ['controller' => 'Articles', 'action' => 'view', $articlesAvailability->article->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($articlesAvailability->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Availability') ?></th>
            <td><?= $this->Number->format($articlesAvailability->availability) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($articlesAvailability->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($articlesAvailability->modified) ?></tr>
        </tr>
    </table>
</div>
