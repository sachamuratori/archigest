<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $articlesAvailability->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $articlesAvailability->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Articles Availability'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="articlesAvailability form large-9 medium-8 columns content">
    <?= $this->Form->create($articlesAvailability) ?>
    <fieldset>
        <legend><?= __('Edit Articles Availability') ?></legend>
        <?php
            echo $this->Form->input('article_id', ['options' => $articles]);
            echo $this->Form->input('availability');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
