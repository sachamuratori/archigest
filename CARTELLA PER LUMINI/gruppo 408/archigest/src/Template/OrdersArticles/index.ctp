<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('New Orders Article'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ordersArticles index large-9 medium-8 columns content">
    <h3><?= __('Orders Articles') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('order_id') ?></th>
                <th><?= $this->Paginator->sort('article_id') ?></th>
                <th><?= $this->Paginator->sort('articlequantity') ?></th>
                <th><?= $this->Paginator->sort('price') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ordersArticles as $ordersArticle): ?>
            <tr>
                <td><?= $this->Number->format($ordersArticle->id) ?></td>
                <td><?= $ordersArticle->has('order') ? $this->Html->link($ordersArticle->order->name, ['controller' => 'Orders', 'action' => 'view', $ordersArticle->order->id]) : '' ?></td>
                <td><?= $ordersArticle->has('article') ? $this->Html->link($ordersArticle->article->name, ['controller' => 'Articles', 'action' => 'view', $ordersArticle->article->id]) : '' ?></td>
                <td><?= h($ordersArticle->articlequantity) ?></td>
                <td><?= $this->Number->format($ordersArticle->price) ?></td>
                <td><?= h($ordersArticle->created) ?></td>
                <td><?= h($ordersArticle->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ordersArticle->id]) ?>
                    <?php if($ordersArticle->order->evaded == "no"){ ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ordersArticle->id]) ?>
                    <?php } ?>
                    <?php if($ordersArticle->order->evaded == "no"){ ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ordersArticle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ordersArticle->id)]) ?>
                    <?php } ?>
                    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
