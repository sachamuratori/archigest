<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$pageName = 'ArchiGest';

$user = $userRole." ".$userName." ".$userSurname;
$action = true;
if($userAction == 'login' || $userAction == 'register'){
    $action = false;
}

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $pageName ?>:
        <?= 'Polyclinic Managment System' ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('my.css') ?>
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <?php if($action){ ?>
                <h1><a><?= $this->fetch('title'); ?></a></h1>
                <?php } ?>
            </li>
        </ul>
        <section class="top-bar-section">
            <ul class="right">
                 <?php if($action){ ?>
                <li><?php   if($userRole === 'Admin'){
                                echo $this->Html->link($user,
                                    ['controller' => 'Users', 'action' => 'indexAdmin', '_full' => true, 'target' => '_blank']
                                );
                            } else if($userRole === 'Doctor' || $userRole === 'Janitor' || $userRole === 'Nurse' || $userRole === 'Client'){
                                echo $this->Html->link($user,
                                    ['controller' => 'Users', 'action' => 'view',$userId, '_full' => true, 'target' => '_blank']
                                );
                            }
                ?></li>
                <?php } ?>
                <?php if($action){ ?>
                <li><?php echo $this->Html->link(
                        'Logout',
                        ['controller' => 'Users', 'action' => 'logout', '_full' => true, 'target' => '_blank']
                    );
                ?></li>
                <?php } ?>
            </ul>
        </section>
    </nav>
    <?= $this->Flash->render() ?>
    <section class="container clearfix">
        <?= $this->fetch('content') ?>
    </section>
    <footer class="footer">
         <ul class="center">
            <li>
            <h6>
                <em>
                Archigest is a software platform for hospitals. Developed by Muratori Sacha & Fabbri Andrea.
                </em>
                <br>
                <em>
                Archigest 2015/2016
                </em>
            </h6>
            </li>
        </ul>
    </footer>   
</body>
</html>
