<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Doctor') ?></li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Treatments'), ['controller' => 'Treatments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Treatment'), ['controller' => 'Treatments', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="medicalrecords form large-9 medium-8 columns content">
    <?= $this->Form->create($medicalrecord) ?>
    <fieldset>
        <legend><?= __('Add Medicalrecord') ?></legend>
        <?php
            echo $this->Form->input('client');
            echo $this->Form->input('client_name');
            echo $this->Form->input('client_surname');
            echo $this->Form->input('client_ct');
            echo $this->Form->input('name');
            echo $this->Form->input('anamnesis');
            echo $this->Form->input('medicalhistory');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
