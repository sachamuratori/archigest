<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bookedexam'), ['controller' => 'Bookedexams', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Treatments'), ['controller' => 'Treatments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Treatment'), ['controller' => 'Treatments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="medicalrecords form large-9 medium-8 columns content">
    <?= $this->Form->create($medicalrecord) ?>
    <fieldset>
        <legend><?= __('Add Medicalrecord') ?></legend>
        <?php
            echo $this->Form->input('client');
            echo $this->Form->input('client_name');
            echo $this->Form->input('client_surname');
            echo $this->Form->input('client_ct');
            echo $this->Form->input('name');
            echo $this->Form->input('anamnesis');
            echo $this->Form->input('medicalhistory');
            echo $this->Form->input('users._ids', ['options' => $users, 'required' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
