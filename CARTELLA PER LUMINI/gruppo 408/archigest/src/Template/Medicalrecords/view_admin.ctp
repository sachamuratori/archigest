<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('Edit Medicalrecord'), ['action' => 'edit', $medicalrecord->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Medicalrecord'), ['action' => 'delete', $medicalrecord->id], ['confirm' => __('Are you sure you want to delete # {0}?', $medicalrecord->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Medicalrecord'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bookedexam'), ['controller' => 'Bookedexams', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Treatments'), ['controller' => 'Treatments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Treatment'), ['controller' => 'Treatments', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="medicalrecords view large-9 medium-8 columns content">
    <h3><?= h($medicalrecord->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($medicalrecord->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Client Id') ?></th>
            <td><?= $this->Number->format($medicalrecord->client_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Client Name') ?></th>
            <td><?= h($medicalrecord->client_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Client Surname') ?></th>
            <td><?= h($medicalrecord->client_surname) ?></td>
        </tr>
        <tr>
            <th><?= __('Client Ct') ?></th>
            <td><?= h($medicalrecord->client_ct) ?></td>
        </tr>
        <tr>
            <th><?= __('Anamnesis') ?></th>
            <td><?= h($medicalrecord->anamnesis) ?></td>
        </tr>
        <tr>
            <th><?= __('Medicalhistory') ?></th>
            <td><?= h($medicalrecord->medicalhistory) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($medicalrecord->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($medicalrecord->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($medicalrecord->modified) ?></tr>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Bookedexams') ?></h4>
        <?php if (!empty($medicalrecord->bookedexams)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Exam Id') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Time') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Medicalrecord Id') ?></th>
                <th><?= __('Dailyturn Id') ?></th>
                <th><?= __('Result') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($medicalrecord->bookedexams as $bookedexams): ?>
            <tr>
                <td><?= h($bookedexams->id) ?></td>
                <td><?= h($bookedexams->exam_id) ?></td>
                <td><?= h($bookedexams->date) ?></td>
                <td><?= h($bookedexams->time) ?></td>
                <td><?= h($bookedexams->user_id) ?></td>
                <td><?= h($bookedexams->medicalrecord_id) ?></td>
                <td><?= h($bookedexams->dailyturn_id) ?></td>
                <td><?= h($bookedexams->result) ?></td>
                <td><?= h($bookedexams->created) ?></td>
                <td><?= h($bookedexams->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bookedexams', 'action' => 'view', $bookedexams->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bookedexams', 'action' => 'edit', $bookedexams->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bookedexams', 'action' => 'delete', $bookedexams->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookedexams->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Treatments') ?></h4>
        <?php if (!empty($medicalrecord->treatments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Medicalrecord Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Startdate') ?></th>
                <th><?= __('Enddate') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($medicalrecord->treatments as $treatments): ?>
            <tr>
                <td><?= h($treatments->id) ?></td>
                <td><?= h($treatments->medicalrecord_id) ?></td>
                <td><?= h($treatments->user_id) ?></td>
                <td><?= h($treatments->name) ?></td>
                <td><?= h($treatments->description) ?></td>
                <td><?= h($treatments->startdate) ?></td>
                <td><?= h($treatments->enddate) ?></td>
                <td><?= h($treatments->created) ?></td>
                <td><?= h($treatments->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Treatments', 'action' => 'view', $treatments->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Treatments', 'action' => 'edit', $treatments->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Treatments', 'action' => 'delete', $treatments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $treatments->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Employeers of this Medicalrecord') ?></h4>
        <?php if (!empty($medicalrecord->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Role') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Password') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Surname') ?></th>
                <th><?= __('Sex') ?></th>
                <th><?= __('Dateofbirth') ?></th>
                <th><?= __('Cf') ?></th>
                <th><?= __('Address') ?></th>
                <th><?= __('City') ?></th>
                <th><?= __('Postalcode') ?></th>
                <th><?= __('Province') ?></th>
                <th><?= __('Country') ?></th>
                <th><?= __('Telephone1') ?></th>
                <th><?= __('Telephone2') ?></th>
                <th><?= __('Medicalrecord Id') ?></th>
                <th><?= __('Ct') ?></th>
                <th><?= __('Type') ?></th>
                <th><?= __('Salary') ?></th>
                <th><?= __('Specialization') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($medicalrecord->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->role) ?></td>
                <td><?= h($users->email) ?></td>
                <td><?= h($users->password) ?></td>
                <td><?= h($users->name) ?></td>
                <td><?= h($users->surname) ?></td>
                <td><?= h($users->sex) ?></td>
                <td><?= h($users->dateofbirth) ?></td>
                <td><?= h($users->cf) ?></td>
                <td><?= h($users->address) ?></td>
                <td><?= h($users->city) ?></td>
                <td><?= h($users->postalcode) ?></td>
                <td><?= h($users->province) ?></td>
                <td><?= h($users->country) ?></td>
                <td><?= h($users->telephone1) ?></td>
                <td><?= h($users->telephone2) ?></td>
                <td><?= h($users->medicalrecord_id) ?></td>
                <td><?= h($users->ct) ?></td>
                <td><?= h($users->type) ?></td>
                <td><?= h($users->salary) ?></td>
                <td><?= h($users->specialization) ?></td>
                <td><?= h($users->user_id) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
