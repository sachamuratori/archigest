<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('Edit Users Medicalrecord'), ['action' => 'edit', $usersMedicalrecord->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Medicalrecord'), ['action' => 'delete', $usersMedicalrecord->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersMedicalrecord->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Medicalrecords'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Medicalrecord'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Medicalrecord'), ['controller' => 'Medicalrecords', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersMedicalrecords view large-9 medium-8 columns content">
    <h3><?= h($usersMedicalrecord->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $usersMedicalrecord->has('user') ? $this->Html->link($usersMedicalrecord->user->name, ['controller' => 'Users', 'action' => 'view', $usersMedicalrecord->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Medicalrecord') ?></th>
            <td><?= $usersMedicalrecord->has('medicalrecord') ? $this->Html->link($usersMedicalrecord->medicalrecord->name, ['controller' => 'Medicalrecords', 'action' => 'view', $usersMedicalrecord->medicalrecord->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($usersMedicalrecord->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($usersMedicalrecord->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($usersMedicalrecord->modified) ?></tr>
        </tr>
    </table>
</div>
