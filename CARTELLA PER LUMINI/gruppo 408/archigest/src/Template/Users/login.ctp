<div class="row">
    <br><br><br>
    <h1>Login</h1>
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Please insert email and password') ?></legend>
        <?= $this->Form->input('email') ?>
        <?= $this->Form->input('password') ?>
    </fieldset>
    <?= $this->Form->button('Login') ?>
    <br>
    <?= $this->Html->link(__('Are you a new client? Register!'), ['controller' => 'Users', 'action' => 'register']) ?>
    <?= $this->Form->end() ?>
</div>