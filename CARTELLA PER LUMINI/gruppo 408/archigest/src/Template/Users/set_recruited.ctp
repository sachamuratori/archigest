<?php
$this->element('sidebars');
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
         <?php echo $this->fetch('sidebar_admin'); ?>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create() ?> 
    <fieldset>
        <legend><?= __('Set the employeer you have recruited') ?></legend>
        <?php
            echo $this->Form->input('employeer', ['options' => $employeers]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
