<?php
$this->element('sidebars');
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
         <?php echo $this->fetch('sidebar_employeer_nurse'); ?>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name), ' ', h($user->surname)?></h3>
        
    <div class="information_general">
        <table class="vertical-table">
            <h5><?= __('General Information') ?></h5>
            <tr>
                <th><?= __('Id') ?></th>
                <td><?= $this->Number->format($user->id) ?></td>
            </tr>
            <tr>
                <th><?= __('Role') ?></th>
                <td><?php 
                        if(($user->role) === 0){
                            echo 'Admin';
                        } else if(($user->role) === 1) {
                            echo 'Employeer';
                        } else if(($user->role) === 2) {
                            echo 'Client';
                        }
                    ?></td>
            </tr>
            <tr>
                <th><?= __('Email') ?></th>
                <td><?= h($user->email) ?></td>
            </tr>
            <tr>
                <th><?= __('Password') ?></th>
                <td><?= h($user->password) ?></td>
            </tr>
        </table>
    </div>
    
    <div class="information_identity">
        <table class="vertical-table">
            <h5><?= __('Identity Information') ?></h5>
            <tr>
                <th><?= __('Name') ?></th>
                <td><?= h($user->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Surname') ?></th>
                <td><?= h($user->surname) ?></td>
            </tr>
            <tr>
                <th><?= __('Dateofbirth') ?></th>
                <td><?= h($user->dateofbirth->format('Y-m-d')) ?></td>
            </tr>
            <tr>
                <th><?= __('Sex') ?></th>
                <td><?php 
                        if(($user->sex) == 0){
                            echo 'M';
                        } else if(($user->sex) == 1) {
                            echo 'F';
                        } else if(($user->sex) == 2) {
                            echo 'Not Specified';
                        }
                ?></td>
            </tr>
            <tr>
                <th><?= __('Cf') ?></th>
                <td><?= h($user->cf) ?></td>
            </tr>
            <tr>
                <th><?= __('Address') ?></th>
                <td><?= h($user->address) ?></td>
            </tr>
            <tr>
                <th><?= __('City') ?></th>
                <td><?= h($user->city) ?></td>
            </tr>
            <tr>
                <th><?= __('Postalcode') ?></th>
                <td><?= h($user->postalcode) ?></td>
            </tr>
            <tr>
                <th><?= __('Province') ?></th>
                <td><?= h($user->province) ?></td>
            </tr>
            <tr>
                <th><?= __('Country') ?></th>
                <td><?= h($user->country) ?></td>
            </tr>
            <tr>
                <th><?= __('Telephone1') ?></th>
                <td><?= h($user->telephone1) ?></td>
            </tr>
            <tr>
                <th><?= __('Telephone2') ?></th>
                <td><?= h($user->telephone2) ?></td>
            </tr>
            <tr>
                <th><?= __('Ct') ?></th>
                <td><?= h($user->ct) ?></td>
            </tr>
        </table>
    </div>

    <div class="information_employeer">
        <table class="vertical-table">
            <h5><?= __('Employeer Information') ?></h5>
            <tr>
                <th><?= __('Type') ?></th>
                <td><?= h($user->type) ?></td>
            </tr>
            <tr>
                <th><?= __('Salary') ?></th>
                <td><?= $this->Number->format($user->salary) ?></td>
            </tr>
            <tr>
                <th><?= __('Recruited by') ?></th>
                <td><?= 'Admin ', h($adminName), ' ',h($adminSurname) ?></td>
            </tr>
        </table>
    </div>
      
    <div class="information_database">
        <table class="vertical-table">
            <tr>
                <th><?= __('Created') ?></th>
                <td><?= h($user->created) ?></tr>
            </tr>
            <tr>
                <th><?= __('Modified') ?></th>
                <td><?= h($user->modified) ?></tr>
            </tr>
        </table>
    </div>
    
    <div class="related">
        <h4><?= __('Related Dailyturns') ?></h4>
        <?php if (!empty($user->dailyturns)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Ward Id') ?></th>
                <th><?= __('Turn Id') ?></th>
                <th><?= __('Bookedexam Id') ?></th>
                <th><?= __('Time') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->dailyturns as $dailyturns): ?>
            <tr>
                <td><?= h($dailyturns->id) ?></td>
                <td><?= h($dailyturns->ward_id) ?></td>
                <td><?= h($dailyturns->turn_id) ?></td>
                <td><?= h($dailyturns->bookedexam_id) ?></td>
                <td><?= h($dailyturns->time->format('H:i')) ?></td>
                <td><?= h($dailyturns->created) ?></td>
                <td><?= h($dailyturns->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Dailyturns', 'action' => 'view', $dailyturns->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
