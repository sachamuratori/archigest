<?php
$this->element('sidebars');
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
         <?php echo $this->fetch('sidebar_admin'); ?>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->input('role', array(
                'options' => array("Admin", "Employeer", "Client")
                ));
            echo $this->Form->input('email');
            echo $this->Form->input('password');
            echo $this->Form->input('name');
            echo $this->Form->input('surname');
            echo $this->Form->input('sex', array(
                'options' => array("M" => "M", "F" => "F", "Not Specified" => "Not Specified")
                ));
            echo $this->Form->input('dateofbirth', array(
                    'label' => 'Date of birth',
                    'dateFormat' => 'DMY',
                    'minYear' => date('Y') - 80,
                    'maxYear' => date('Y') - 14,
                ));
            echo $this->Form->input('cf');
            echo $this->Form->input('address');
            echo $this->Form->input('city');
            echo $this->Form->input('postalcode');
            echo $this->Form->input('province');
            echo $this->Form->input('country');
            echo $this->Form->input('telephone1');
            echo $this->Form->input('telephone2');
            echo $this->Form->input('medicalrecord_id', array(
                'empty' => true
                ));
            echo $this->Form->input('ct');
            echo $this->Form->input('type', array(
                'options' => array(NULL, "Doctor" => "Doctor", "Nurse" => "Nurse", "Janitor" => "Janitor")
                ));
            echo $this->Form->input('salary');
            echo $this->Form->input('specialization');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
