<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $turn->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $turn->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Turns'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dailyturn'), ['controller' => 'Dailyturns', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="turns form large-9 medium-8 columns content">
    <?= $this->Form->create($turn) ?>
    <fieldset>
        <legend><?= __('Edit Turn') ?></legend>
        <?php
            echo $this->Form->input('date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
