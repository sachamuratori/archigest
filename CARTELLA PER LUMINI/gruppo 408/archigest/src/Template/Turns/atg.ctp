<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('List Turns'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dailyturn'), ['controller' => 'Dailyturns', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="turns form large-9 medium-8 columns content">
    <?= $this->Form->create() ?> 
    <fieldset>
        <legend><?= __('Please insert the year. It will be generated every turn(every day) of that year.') ?></legend>
        <?php
            echo 'Year';
            echo $this->Form->year('year', [
                'minYear' => date('Y'),
                'maxYear' => date('Y', strtotime('+2 years'))
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
