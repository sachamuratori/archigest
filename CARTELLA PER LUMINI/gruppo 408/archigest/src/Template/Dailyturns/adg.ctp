<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('List Turns'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="turns form large-9 medium-8 columns content">
    <?= $this->Form->create() ?> 
    <fieldset>
        <legend><?= __('Please insert the number of day(s). It will be generated every dailyturn for each employeer for the number of day(s).') ?></legend>
        <?php
            echo "Max number of day(s) is '31'";
            echo $this->Form->Number('day(s)');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
