<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Wards Controller
 *
 * @property \App\Model\Table\WardsTable $Wards
 */
class WardsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('wards', $this->paginate($this->Wards));
        $this->set('_serialize', ['wards']);
        
        /*Choose the right index */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * View method
     *
     * @param string|null $id Ward id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ward = $this->Wards->get($id, [
            'contain' => ['Exams', 'Rooms', 'Warehouses']
        ]);
        $this->set('ward', $ward);
        $this->set('_serialize', ['ward']);
        
        /*Choose the right view */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ward = $this->Wards->newEntity();
        if ($this->request->is('post')) {
            $ward = $this->Wards->patchEntity($ward, $this->request->data);
            if ($this->Wards->save($ward)) {
                $this->Flash->success(__('The ward has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ward could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ward'));
        $this->set('_serialize', ['ward']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ward id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ward = $this->Wards->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ward = $this->Wards->patchEntity($ward, $this->request->data);
            if ($this->Wards->save($ward)) {
                $this->Flash->success(__('The ward has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ward could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('ward'));
        $this->set('_serialize', ['ward']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ward id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ward = $this->Wards->get($id);
        if ($this->Wards->delete($ward)) {
            $this->Flash->success(__('The ward has been deleted.'));
        } else {
            $this->Flash->error(__('The ward could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role === 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role === 1 || $role === 2){ //EMPLOYEER AND CLIENT AUTHORIZATION 
            if (in_array($this->request->action, ['view', 'index'])) {
                 return true;
            } else if (in_array($this->request->action, ['add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Wards', 'action' => 'index']);
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
}
