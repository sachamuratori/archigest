<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersExams Controller
 *
 * @property \App\Model\Table\UsersExamsTable $UsersExams
 */
class UsersExamsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Exams']
        ];
        $this->set('usersExams', $this->paginate($this->UsersExams));
        $this->set('_serialize', ['usersExams']);
    }

    /**
     * View method
     *
     * @param string|null $id Users Exam id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersExam = $this->UsersExams->get($id, [
            'contain' => ['Users', 'Exams']
        ]);
        $this->set('usersExam', $usersExam);
        $this->set('_serialize', ['usersExam']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersExam = $this->UsersExams->newEntity();
        if ($this->request->is('post')) {
            $usersExam = $this->UsersExams->patchEntity($usersExam, $this->request->data);
            if ($this->UsersExams->save($usersExam)) {
                $this->Flash->success(__('The users exam has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users exam could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersExams->Users->find('list', ['limit' => 200])->where(['role' => 1])->where(['type' => "Doctor"]);
        $exams = $this->UsersExams->Exams->find('list', ['limit' => 200]);
        $this->set(compact('usersExam', 'users', 'exams'));
        $this->set('_serialize', ['usersExam']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Exam id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersExam = $this->UsersExams->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersExam = $this->UsersExams->patchEntity($usersExam, $this->request->data);
            if ($this->UsersExams->save($usersExam)) {
                $this->Flash->success(__('The users exam has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users exam could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersExams->Users->find('list', ['limit' => 200])->where(['role' => 1])->where(['type' => 'Doctor']);
        $exams = $this->UsersExams->Exams->find('list', ['limit' => 200]);
        $this->set(compact('usersExam', 'users', 'exams'));
        $this->set('_serialize', ['usersExam']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Exam id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersExam = $this->UsersExams->get($id);
        if ($this->UsersExams->delete($usersExam)) {
            $this->Flash->success(__('The users exam has been deleted.'));
        } else {
            $this->Flash->error(__('The users exam could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role == 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role == 1 || $role == 2){ //EMPLOYEER AND CLIENT AUTHORIZATION
            
            if (in_array($this->request->action, ['view', 'index', 'add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]); 
            } else {
                $this->Flash->error(__('Error in role Authorization process'));
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
}
