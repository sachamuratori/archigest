<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Turns Controller
 *
 * @property \App\Model\Table\TurnsTable $Turns
 */
class TurnsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $role = $this->Auth->user("role");
        if($role == 0){
            $this->set('turns', $this->Paginator->paginate($this->Turns));
            $this->set(compact('turns'));
            $this->set('_serialize', ['turns']);
        } else {
            $start_date = date('Y-m-d');
            $end_date = date('Y-m-d', strtotime($start_date . ' +30 day'));
            $this->set('turns', $this->Paginator->paginate($this->Turns, [
                'conditions' => [
                    "Turns.date BETWEEN '$start_date' AND '$end_date'"
                ],
                'limit' => 31
            ]));
            $this->set(compact('turns'));
            $this->set('_serialize', ['turns']);
        }
        /*Choose the right index */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * View method
     *
     * @param string|null $id Turn id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $turn = $this->Turns->get($id, [
            'contain' => ['Dailyturns']
        ]);
        $this->set('turn', $turn);
        $this->set('_serialize', ['turn']);
        
        /*Choose the right view */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $turn = $this->Turns->newEntity();
        if ($this->request->is('post')) {
            
            $turn = $this->Turns->patchEntity($turn, $this->request->data);
            if ($this->Turns->save($turn)) {
                $this->Flash->success(__('The turn has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The turn could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('turn'));
        $this->set('_serialize', ['turn']);
        
        /*Choose the right add */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Edit method
     *
     * @param string|null $id Turn id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $turn = $this->Turns->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $turn = $this->Turns->patchEntity($turn, $this->request->data);
            if ($this->Turns->save($turn)) {
                $this->Flash->success(__('The turn has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The turn could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('turn'));
        $this->set('_serialize', ['turn']);
        
        /*Choose the right edit */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Delete method
     *
     * @param string|null $id Turn id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $turn = $this->Turns->get($id);
        if ($this->Turns->delete($turn)) {
            $this->Flash->success(__('The turn has been deleted.'));
        } else {
            $this->Flash->error(__('The turn could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role === 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role === 1){ //EMPLOYEER AUTHORIZATION 
            if (in_array($this->request->action, ['view', 'index'])) {
                 return true;
            } else if (in_array($this->request->action, ['add', 'edit', 'delete','atg'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Turns', 'action' => 'index']);
            }
        } else if($role === 2){ //CLIENT AUTHORIZATION 
            if (in_array($this->request->action, ['view', 'index','add', 'edit', 'delete','atg'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]);
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
    
    
    public function atg(){ //Automatic Turns Generator 
        if ($this->request->is('post')){
            if($this->request->data['year']['year'] == ''){
                $this->Flash->error(__('Please select the year and try again'));
                return $this->redirect(['action' => 'atg']);
            }
            
            $months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
            $days1 = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20',
                     '21','22','23','24','25','26','27','28','29','30','31'];
            $days2 = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20',
                     '21','22','23','24','25','26','27','28','29','30'];
            $days3 = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20',
                     '21','22','23','24','25','26','27','28','29'];
            $days4 = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20',
                     '21','22','23','24','25','26','27','28'];
            $year = $this->request->data['year']['year'];
            
            foreach($months as $month){
                debug($month);
                if($month == '02'){
                    if((((int)$year) % 4) == 0){ //bisestile
                        foreach($days3 as $day){
                            $data = [   'date' => [
                                            'year' => $year,
                                            'month' => $month,
                                            'day' => $day
                                                ]
                                    ];
                            $this->savingTurn($data);
                        }
                    } else {
                        foreach($days4 as $day){
                            $data = [   'date' => [
                                            'year' => $year,
                                            'month' => $month,
                                            'day' => $day
                                                ]
                                    ];
                            $this->savingTurn($data);
                        }
                    }
                } else if($month == '04' || $month == '06' || $month == '09' ||$month == '11'){
                    foreach($days2 as $day){
                        $data = [   'date' => [
                                        'year' => $year,
                                        'month' => $month,
                                        'day' => $day
                                            ]
                                ];
                        $this->savingTurn($data);
                    }
                } else {
                    foreach($days1 as $day){
                        $data = [   'date' => [
                                        'year' => $year,
                                        'month' => $month,
                                        'day' => $day
                                            ]
                                ];
                        $this->savingTurn($data);
                    }
                }
            }
            //die;
            return $this->redirect(['action' => 'index']);
        }
    }
    
    private function savingTurn($data){ //Automatic Turns Generator
        $this->request->data = $data;
        $turn = $this->Turns->newEntity();
        $turn = $this->Turns->patchEntity($turn, $this->request->data);
            
        if ($this->Turns->save($turn)) {
        } else {
            $this->Flash->error(__('The turn could not be saved. Please Delete all the turns you have created'));
        }
    }
}
