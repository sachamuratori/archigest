<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Medicalrecords Controller
 *
 * @property \App\Model\Table\MedicalrecordsTable $Medicalrecords
 */
class MedicalrecordsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if($this->Auth->user("role") == 1){ //employeer doctor o nurse
            //inpagina solo le cartelle cliniche che hanno un riferimento in users_medicalrecords
            //cerca i dailyturn associati a quel dottore
            $users_medicalrecordsTable = TableRegistry::get('UsersMedicalrecords');
            $querysql = $users_medicalrecordsTable->find('all');
            $users_medicalrecords = $querysql
                                ->select(['medicalrecord_id'])
                                ->where(['user_id' => $this->Auth->user("id")])
                                ->toArray();
            
            $ccs = [];
            foreach($users_medicalrecords as $users_medicalrecord){
                array_push($ccs,$users_medicalrecord['medicalrecord_id']);
            }
            //in real_dailyturns ci sono gli id di tutti i turni di quel dottore 
            //di una settimana a partire da oggi 
            if($ccs != null){
                $id_first = $ccs[0];
                $id_end = end($ccs);
                if($id_first == $id_end){
                    $query = "Medicalrecords.id IN ('$ccs[0]')"; 
                } else {
                    $query = "Medicalrecords.id IN ('$ccs[0]',"; 

                    foreach($ccs as $cc){
                        if($id_first == $cc){
                            continue;
                        }
                        if($cc == $id_end){
                            $query = $query." '$cc')";
                        } else {
                            $query = $query." '$cc',";
                        }
                    }
                }
                $this->set('medicalrecords', $this->Paginator->paginate($this->Medicalrecords, [
                                    'conditions' => [
                                                $query
                                            ],
                                    'limit' => 100
                ]));
                $this->set(compact('medicalrecords'));
                $this->set('_serialize', ['medicalrecords']);
                
            } else {
                $query = "Medicalrecords.id IN (null)"; 
                $this->set('medicalrecords', $this->Paginator->paginate($this->Medicalrecords, [
                                    'conditions' => [
                                                $query
                                            ],
                                    'limit' => 100
                ]));
                $this->set(compact('medicalrecords'));
                $this->set('_serialize', ['medicalrecords']);
            }
            
        } else {
            $this->set('medicalrecords', $this->paginate($this->Medicalrecords));
            $this->set('_serialize', ['medicalrecords']);
        }
        /*Choose the right index */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * View method
     *
     * @param string|null $id Medicalrecord id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $medicalrecord = $this->Medicalrecords->get($id, [
            'contain' => ['Users', 'Bookedexams', 'Treatments']
        ]);
        $this->set('medicalrecord', $medicalrecord);
        $this->set('_serialize', ['medicalrecord']);
        
        /*Choose the right view */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $medicalrecord = $this->Medicalrecords->newEntity();
        if ($this->request->is('post')) {
            
            /* */
            $data = $this->change_client_to_clientid($this->request->data);
            $this->request->data = $data;
            /* */
            
            if($this->Auth->user('role') == 1){ 
                $users = ['users' => [
                            '_ids' => [
                                (int) 0 => $this->Auth->user('id')
                         ]]];
                $data = $this->request->data;
                $result = array_merge($data,$users);
                $this->request->data = $result;
            }
            
            $medicalrecord = $this->Medicalrecords->patchEntity($medicalrecord, $this->request->data);
            if ($this->Medicalrecords->save($medicalrecord)) {
                /* */
                $this->set_medicalrecord_to_client($medicalrecord['client_id'],$medicalrecord['id']);
                /* */
                /*$this->add_usersmedicalrecords($this->request->data['users'], $medicalrecord['id']);
                LO FA DA SOLO*/
                $this->Flash->success(__('The medicalrecord has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The medicalrecord could not be saved. Please, try again.'));
            }
        }
        if($this->Auth->user('role') == 1){ //è un dottore perchè janitor e nurse non possono accedervi
            $users = $this->Medicalrecords->Users->find('list')->where(['id' => $this->Auth->user('id')]);
        } else {
            $users = $this->Medicalrecords->Users->find('list', ['limit' => 200])->where(['type' => "Doctor"]); 
        }
        $this->set(compact('medicalrecord', 'users'));
        $this->set('_serialize', ['medicalrecord']);
        
        /*Choose the right add */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Edit method
     *
     * @param string|null $id Medicalrecord id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $medicalrecord = $this->Medicalrecords->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            /* */
            $data = $this->change_client_to_clientid($this->request->data);
            $this->request->data = $data;
            /* */
            
            if($this->Auth->user('role') == 1){ 
                $users = ['users' => [
                            '_ids' => [
                                (int) 0 => $this->Auth->user('id')
                         ]]];
                $data = $this->request->data;
                $result = array_merge($data,$users);
                $this->request->data = $result;
            }
            
            $medicalrecord = $this->Medicalrecords->patchEntity($medicalrecord, $this->request->data);
            if ($this->Medicalrecords->save($medicalrecord)) {
                $this->Flash->success(__('The medicalrecord has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The medicalrecord could not be saved. Please, try again.'));
            }
        }
        if($this->Auth->user('role') == 1){ //è un dottore perchè janitor e nurse non possono accedervi
            $users = $this->Medicalrecords->Users->find('list')->where(['id' => $this->Auth->user('id')]);
        } else {
            $users = $this->Medicalrecords->Users->find('list', ['limit' => 200])->where(['type' => "Doctor"]); 
        }
        $this->set(compact('medicalrecord', 'users'));
        $this->set('_serialize', ['medicalrecord']);
        
        /*Choose the right edit */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Delete method
     *
     * @param string|null $id Medicalrecord id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $medicalrecord = $this->Medicalrecords->get($id);
        if ($this->Medicalrecords->delete($medicalrecord)) {
            $this->Flash->success(__('The medicalrecord has been deleted.'));
        } else {
            $this->Flash->error(__('The medicalrecord could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        $type =  $this->Auth->user("type");
        if($role == 0){
            return parent::isAuthorized($user);
            
        } else if(($role == 1) && (strcmp($type,'Doctor') == 0)){ //EMPLOYEER DOCTOR AUTHORIZATION  
            if (in_array($this->request->action, ['add','index'])) {
                 return true; 
            } else if (in_array($this->request->action, ['view','edit','delete'])) {
                $usersMedicalrecordsTable = TableRegistry::get('UsersMedicalrecords');
                $query = $usersMedicalrecordsTable->find();
                $users = $query->select(['user_id'])->where(['medicalrecord_id' => $this->request->params['pass'][0]])->toArray();
                $ok = false;
                foreach($users as &$a){
                    if($a['user_id'] == $user['id']){
                        $ok = true;
                    }
                }
                if($ok){
                    return true;
                } else {
                    $this->Flash->error(__('You cannot access that location. This is not one of your medicalrecords'));
                    $this->redirect(['controller' => 'Medicalrecords', 'action' => 'index']);
                }
            }
            
        } else if(($role == 1) && (strcmp($type,'Nurse') == 0)){ //EMPLOYEER NURSE AUTHORIZATION
            if (in_array($this->request->action, ['index'])) {
                 return true; 
            } else if (in_array($this->request->action, ['view'])) {
                $usersMedicalrecordsTable = TableRegistry::get('UsersMedicalrecords');
                $query = $usersMedicalrecordsTable->find();
                $users = $query->select(['user_id'])->where(['medicalrecord_id' => $this->request->params['pass'][0]])->toArray();
                $ok = false;
                foreach($users as &$a){
                    if($a['user_id'] == $user['id']){
                        $ok = true;
                    }
                }
                if($ok){
                    return true;
                } else {
                    $this->Flash->error(__('You cannot access that location. This is not one of your medicalrecords'));
                    $this->redirect(['controller' => 'Medicalrecords', 'action' => 'index']);
                }
            } else if (in_array($this->request->action, ['add','edit','delete'])) {
                    $this->Flash->error(__('You cannot access that location'));
                    $this->redirect(['controller' => 'Medicalrecords', 'action' => 'index']);
            }
        } else if(($role == 1) && (strcmp($type,'Janitor') == 0)){ //EMPLOYEER JANITOR AUTHORIZATION  
            if (in_array($this->request->action, ['index','view','add','edit','delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]);
            }
        } else if($role == 2){ //CLIENT AUTHORIZATION
            if (in_array($this->request->action, ['index','add','edit','delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]);
                
            } else if (in_array($this->request->action, ['view'])) {
                $medicalrecord_id = $this->request->params['pass'][0];
                if ($medicalrecord_id == $user['medicalrecord_id']) {
                    return true;
                } else {
                    $this->Flash->error(__('You cannot access that location'));
                    $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]);
                }
            }
            
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
    
    private function change_client_to_clientid($data){
        $data['client_id'] = $data['client'];
        unset($data['client']);
        //debug($data);
        $a = array_splice($data,6);
        $result = $a+$data;
        return $result;
    }
    
    private function set_medicalrecord_to_client($client_id,$medicalrecord_id){
        $usersTable = TableRegistry::get('Users');
        $query = $usersTable->query();
        $query->update()
            ->set(['medicalrecord_id' => $medicalrecord_id])
            ->where(['id' => $client_id])
            ->execute();
    }
    
    /*private function add_usersmedicalrecords($user,$medicalrecord){
        $users_medicalrecordsTable = TableRegistry::get('UsersMedicalrecords');
        $query = $users_medicalrecordsTable->query();
        $query->insert(['user_id','medicalrecord_id'])->values(['user_id' => $user['_ids'][0], 'medicalrecord_id' => $medicalrecord])->execute();

    }*/
    
}
