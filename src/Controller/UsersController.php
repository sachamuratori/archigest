<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['login', 'register', 'logout']);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function indexAdmin()
    {
        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Medicalrecords', 'Dailyturns', 'Exams'/*, 'Users'*/, 'Bookedexams', 'Orders', 'Treatments']
        ]);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
        
        $uzerstable = TableRegistry::get('Users');
        $query0 = $uzerstable->find();
        $person = $query0->select(['role'])->where(['id' => $id])->first();
        if($person['role'] != 2){
            $uzerz = TableRegistry::get('Users');
            $query = $uzerz->find();
            $admin = $query->select(['user_id'])->where(['id' => $id])->first();
            $query2 = $uzerz->find();
            $userName = $query2->select(['name'])->where(['id' => $admin['user_id']])->first();
            $query3 = $uzerz->find();
            $userSurname = $query3->select(['surname'])->where(['id' => $admin['user_id']])->first();
        } else {
            $uzerz = TableRegistry::get('Users');
            $userName = ['name' => null];
            $userSurname = ['surname' => null];
        }
        
        $this->set('adminName', $userName['name']);
        $this->set('adminSurname', $userSurname['surname']);
        
        $query4 = $uzerz->find();
        $client = $query4->select(['role'])->where(['id' => $id])->first();
    
        if(($this->Auth->user("role") == 1) && ($client['role'] == 2)){ 
        //solo nel caso un impiegato doctor o nurse vuole vedere il profilo di un cliente 
            $methodName = __FUNCTION__.'_client';
        } else {
            /*Choose the right view */
            $methodName = __FUNCTION__;
        }
        
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }
    
    public function addAdmin()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user("id");
            
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index_admin']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $medicalrecords = $this->Users->Medicalrecords->find('list', ['limit' => 200]);
        $dailyturns = $this->Users->Dailyturns->find('list', ['limit' => 200]);
        $exams = $this->Users->Exams->find('list', ['limit' => 200]);
        $users = $this->Users->Users->find('list', ['limit' => 200])
                ->where(['Users.role' => 0]);
        
        $this->set(compact('user', 'medicalrecords', 'dailyturns', 'exams','users'));
        $this->set('_serialize', ['user']);
    }
    
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Medicalrecords', 'Dailyturns', 'Exams'/*, 'Users'*/]
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['user_id'] = $this->Auth->user("id");
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $medicalrecords = $this->Users->Medicalrecords->find('list', ['limit' => 200]);
        $dailyturns = $this->Users->Dailyturns->find('list', ['limit' => 200]);
        $exams = $this->Users->Exams->find('list', ['limit' => 200]);
        /*$users = $this->Users->Users->find('list', ['limit' => 200])
                ->where(['Users.role' => 0]);*/
        
        $this->set(compact('user', 'medicalrecords', 'dailyturns', 'exams'/*, 'users'*/));
        $this->set('_serialize', ['user']);
        $this->set('user_id', $this->Auth->user("id"));
        /*Choose the right edit */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName);
        /* */
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index_admin']); //solo l'admin può eliminarsi
    }
    
    public function login()
    {
        if ($this->request->is('post')) {
           
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                if($user['role'] === 0){
                    return $this->redirect(['controller' => 'Users', 'action' => 'index_admin']);
                } elseif($user['role'] === 1){
                    return $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
                } elseif($user['role'] === 2){                 
                    return $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
                }
                $this->Flash->error(__('Error, could not find the role of the user, try again'));
                
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }
    
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function register()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['role'] = 2;
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $medicalrecords = $this->Users->Medicalrecords->find('list', ['limit' => 200]);
        $dailyturns = $this->Users->Dailyturns->find('list', ['limit' => 200]);
        $exams = $this->Users->Exams->find('list', ['limit' => 200]);
        $this->set(compact('user', 'medicalrecords', 'dailyturns', 'exams'));
        $this->set('_serialize', ['user']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        $type =  $this->Auth->user("type");
        if($role == 0){
            return parent::isAuthorized($user);
            
        } else if($role == 1 && $type != "Janitor"){ //cosa può fare un impiegato?
            if (in_array($this->request->action, ['edit'])) {
                $requestId = (int)$this->request->params['pass'][0];
                if ($requestId == $user['id']) {
                    return true;
                } else {
                    $this->Flash->error(__('You cannot access that location'));
                    $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
                }
            } else if (in_array($this->request->action, ['view'])) {
                $requestId = (int)$this->request->params['pass'][0];
                $usersTable = TableRegistry::get('Users');
                $query = $usersTable->find();
                $userisclient = $query->select(['role'])->where(['id' => $requestId])->first();
                if (($user['type'] != "Janitor") && ($requestId == $user['id']) || ($userisclient['role'] == 2)) {
                    return true;
                } else {
                    $this->Flash->error(__('You cannot access that location'));
                    $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
                }
            } else if(in_array($this->request->action, ['add','delete','setRecruited'])){
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
            }
            
        } else if($role == 1 && $type == "Janitor"){ //cosa può fare un impiegato? 
            if (in_array($this->request->action, ['edit'])) {
                $requestId = (int)$this->request->params['pass'][0];
                if ($requestId == $user['id']) {
                    return true;
                } else {
                    $this->Flash->error(__('You cannot access that location'));
                    $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
                }
            } else if (in_array($this->request->action, ['view'])) {
                $requestId = (int)$this->request->params['pass'][0];
                if (($requestId == $user['id'])) {
                    return true;
                } else {
                    $this->Flash->error(__('You cannot access that location'));
                    $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
                }
            } else if(in_array($this->request->action, ['add','delete','setRecruited'])){
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
            }
        } else if($role == 2){ //cosa può fare un cliente?
            if (in_array($this->request->action, ['view','edit', 'delete'])) {
                $requestId = (int)$this->request->params['pass'][0];
                if ($requestId == $user['id']) {
                    return true;
                } else {
                    $this->Flash->error(__('You cannot access that location'));
                    $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
                }
            } else if(in_array($this->request->action, ['add','index','setRecruited'])){
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view', $user['id']]);
            }
            
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
    
    public function setRecruited()
    {
        if ($this->request->is('post')) {
            $usersTable = TableRegistry::get('Users');
            $user = $usersTable->get($this->request->data['employeer']);

            $user->user_id = $this->Auth->user("id");
            $save = $usersTable->save($user); 
            if ($save) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index_admin']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        
        $employeers = $this->Users->Users->find('list')
                ->where(['Users.role' => 1]);
        $this->set(compact('user', 'employeers'));
        $this->set('_serialize', ['user']);
    }
}
