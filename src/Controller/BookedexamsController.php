<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Bookedexams Controller
 *
 * @property \App\Model\Table\BookedexamsTable $Bookedexams
 */
class BookedexamsController extends AppController
{
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Exams', 'Users', 'Medicalrecords']
        ];
        $this->set('bookedexams', $this->paginate($this->Bookedexams));
        $this->set('_serialize', ['bookedexams']);
        
        /*Choose the right index */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName); /*$this->ProcessExamsView($methodName);*/
        /* */
    }

    /**
     * View method
     *
     * @param string|null $id Bookedexam id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bookedexam = $this->Bookedexams->get($id, [
            'contain' => ['Exams', 'Users', 'Medicalrecords', 'Dailyturns']
        ]);
        $this->set('bookedexam', $bookedexam);
        $this->set('_serialize', ['bookedexam']);
        
        /*Choose the right view */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName); /*$this->ProcessExamsView($methodName);*/
        /* */
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bookedexam = $this->Bookedexams->newEntity();
        if ($this->request->is('post')) {
            $data = $this->add_medicalrecord($this->request->data);
            $this->request->data = $data;
            /*debug($this->request->data);*/
            $check = $this->check_date($this->request->data);
            if(!$check){
                $this->Flash->error(__('Please select a future date: starting by tomorrow'));
            } else {
                $already_save_exam = $this->check_saves($this->request->data);
                //debug($already_save_exam);

                if($already_save_exam){
                    $this->Flash->error(__('You have already booked an exam that day.'));
                } else {
                    $a = $this->check_hours($this->request->data);
                    if(array_slice($a,0,1)[0]){ //ci deve essere true
                        $bookedexam = $this->Bookedexams->patchEntity($bookedexam, $this->request->data);
                        if ($this->Bookedexams->save($bookedexam)) {
                            $this->update_dailyturns($bookedexam);
                            $this->Flash->success(__('The bookedexam has been saved.'));
                            if($this->Auth->user('role') == 2){
                                return $this->redirect(['action' => 'view',$bookedexam['id']]);
                            } else {
                                return $this->redirect(['action' => 'index']);
                            }
                        } else {
                            $this->Flash->error(__('The bookedexam could not be saved. Please, try again.'));
                        }
                    } else {
                        //funzione che mi dà una stringa con gli orari disponibili
                        $available_hours = implode(",",array_slice($a,1,1)[0]);
                        $this->Flash->error(__('The bookedexam could not be saved because that hour is not inside the working hours or is full.'
                                . ' Available hours: '.$available_hours));
                    }
                }
            }
        }
        
        $exams = $this->Bookedexams->Exams->find('list', ['limit' => 200]);
        if($this->Auth->user('role') == 2){
            $users = $this->Bookedexams->Users->find('list')->where(['id' => $this->Auth->user('id')]);
        } else {
            $users = $this->Bookedexams->Users->find('list', ['limit' => 200])->where(['role' =>
                2]);
        }
        
        /*$medicalrecords = $this->Bookedexams->Medicalrecords->find('list', ['limit' => 200]);*/
        $this->set(compact('bookedexam', 'exams', 'users'/*, 'medicalrecords'*/));
        $this->set('_serialize', ['bookedexam']);
        
        /*Choose the right add */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName); /*$this->ProcessExamsView($methodName);*/
        /* */
    }

    /**
     * Edit method
     *
     * @param string|null $id Bookedexam id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bookedexam = $this->Bookedexams->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->add_medicalrecord($this->request->data);
            $this->request->data = $data;
            
            $check = $this->check_date($this->request->data);
            if(!$check){
                $this->Flash->error(__('Please select a future date: starting by tomorrow'));
            } else {
                $already_save_exam = $this->check_saves($this->request->data);

                if($already_save_exam){
                    $this->Flash->error(__('You have already booked an exam that day.'));
                } else {
                    $a = $this->check_hours($this->request->data);
                    if(array_slice($a,0,1)[0]){ //ci deve essere true
                        $bookedexam = $this->Bookedexams->patchEntity($bookedexam, $this->request->data);
                        if ($this->Bookedexams->save($bookedexam)) {
                            $this->update_dailyturns($bookedexam);
                            $this->Flash->success(__('The bookedexam has been saved.'));
                            if($this->Auth->user('role') == 2){
                                return $this->redirect(['action' => 'view',$bookedexam['id']]);
                            } else {
                                return $this->redirect(['action' => 'index']);
                            }
                        } else {
                            $this->Flash->error(__('The bookedexam could not be saved. Please, try again.'));
                        }
                    } else {
                        //funzione che mi dà una stringa con gli orari disponibili
                        $available_hours = implode(",",array_slice($a,1,1)[0]);
                        $this->Flash->error(__('The bookedexam could not be saved because that hour is not inside the working hours or is full.'
                                . ' Available hours: '.$available_hours));
                    }
                }
            }
        }
        $exams = $this->Bookedexams->Exams->find('list', ['limit' => 200]);
        if($this->Auth->user('role') == 2){
            $users = $this->Bookedexams->Users->find('list')->where(['id' => $this->Auth->user('id')]);
        } else {
            $users = $this->Bookedexams->Users->find('list', ['limit' => 200])->where(['role' =>
                2]);
        }
        $this->set(compact('bookedexam', 'exams', 'users'));
        $this->set('_serialize', ['bookedexam']);
        
        /*Choose the right edit */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName); /*$this->ProcessExamsView($methodName);*/
        /* */
    }

    /**
     * Delete method
     *
     * @param string|null $id Bookedexam id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bookedexam = $this->Bookedexams->get($id);
        if ($this->Bookedexams->delete($bookedexam)) {
            $this->Flash->success(__('The bookedexam has been deleted.'));
        } else {
            $this->Flash->error(__('The bookedexam could not be deleted. Please, try again.'));
        }
        if($this->Auth->user("role") == 0 || $this->Auth->user("role") == 1){
            return $this->redirect(['action' => 'index']);
        } else {
            return $this->redirect(['controller' => 'Exams', 'action' => 'index']);
        }
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role == 0){
            return parent::isAuthorized($user);
            
        } else if($role == 1){ //EMPLOYEER AUTHORIZATION  
            if (in_array($this->request->action, ['index','view'])) {
                 return true; 
            } else if(in_array($this->request->action, ['add','edit','delete'])){
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Bookedexams', 'action' => 'index']);
            }
            
        } else if($role == 2){ //CLIENT AUTHORIZATION
            if (in_array($this->request->action, ['add'])) {
                 return true; 
            } else if (in_array($this->request->action, ['view','delete'])) {
                //solo se l'esame prenotato è del cliente = bookedexams.user_id = $user['id]
                $bookedexam_id = $this->request->params['pass'][0];
                //carico la tabella bookedexams e faccio una query su quell'id prendendo l'user id
                $bookedexams = TableRegistry::get('Bookedexams');
                $query = $bookedexams->find();
                $result = $query->select(['user_id'])->where(['id' =>
                $bookedexam_id])->first();
                if ($result['user_id'] === $user['id']) {
                    return true;
                    
                } else {
                    $this->Flash->error(__('You cannot access that location'));
                    $this->redirect(['controller' => 'Exams', 'action' => 'index']);
                }
                
            } else if (in_array($this->request->action, ['index','edit'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Exams', 'action' => 'index']);
            }
            
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
    
    private function update_dailyturns($bookedexam){
        /*debug($bookedexam);*/
        
        $users_examsTable = TableRegistry::get('UsersExams');
        $query = $users_examsTable->find('all');
        $enable_docs = $query
                    ->select(['user_id'])
                    ->where(['exam_id' => $bookedexam['exam_id']])
                    ->toArray();
        
        /*debug($enable_docs);*/
        
        $turn = $this->find_turn($bookedexam['date']);
        
        $dailyturnsTable = TableRegistry::get('Dailyturns');
        $query2 = $dailyturnsTable->find('all');
        $dailyturns = $query2
                        ->select([])
                        ->where(['turn_id' => $turn])
                        ->where(['time' => $bookedexam['time']])
                        ->toArray();
        
        $dailyturns_ids = [];
        foreach($dailyturns as $dailyturn){
            array_push($dailyturns_ids,$dailyturn['id']);
        }
        /*debug($dailyturns_ids);*/
        
        if($dailyturns_ids != null){
            $id_first = $dailyturns_ids[0];
            $id_end = end($dailyturns_ids);

            $final_dailyturn = null;
            //controlla che il dottore scelto casualmente non sia già stato usato altrimenti rifà il ciclo
            $stay = 1; $a = 1;
            while($stay == 1){
                //debug('dentro il while. giorno = '.$day.' ora = '.$hour);
                $rand_key = array_rand($enable_docs);   
                //debug('random key = '.$rand_key);
                /*debug($enable_docs[$rand_key]['user_id']);*/

                $doc_id = $enable_docs[$rand_key]['user_id'];

                $query_sql_1 = "UsersDailyturns.user_id = '$doc_id'";
                if($id_first == $id_end){
                    $query_sql_2 = " AND UsersDailyturns.dailyturn_id IN ('$dailyturns_ids[0]')";
                } else {
                    $query_sql_2 = " AND UsersDailyturns.dailyturn_id IN ('$dailyturns_ids[0]',";
                    foreach($dailyturns_ids as $dailyturns_id){
                        if($id_first == $dailyturns_id){
                            continue;
                        }
                        if($id_end == $dailyturns_id){
                            $query_sql_2 = $query_sql_2." '$dailyturns_id')";
                        } else {
                            $query_sql_2 = $query_sql_2." '$dailyturns_id',";
                        }
                    }
                }
                $query_sql_final = $query_sql_1.$query_sql_2;
                /*debug($query_sql_final);*/

                $users_dailyturnsTable = TableRegistry::get('UsersDailyturns');
                $query3 = $users_dailyturnsTable->find('all', array(
                        'conditions' => [
                            $query_sql_final
                        ]
                ));
                $dailyturn = $query3->toArray();

               /* debug($dailyturn[0]['dailyturn_id']);*/

                //guarda se il turno giornaliero ha esame prenotato, se si ricomincia se no ok modifica e salva quel turno
                $DailyturnsTable = TableRegistry::get('Dailyturns');
                $query4 = $DailyturnsTable->find('all');
                $final_dailyturn = $query4
                            ->select([])
                            ->where(['id' => $dailyturn[0]['dailyturn_id']])
                            ->toArray();

                debug($final_dailyturn);
                if($final_dailyturn[0]['bookedexam_id'] != null){
                    $a = 0;
                }
                if($a == 1){
                    $stay = 0;
                }
            }   

            //cerco l'id del reparto associato a quell'esame
            $ExamsTable = TableRegistry::get('Exams');
            $query5 = $ExamsTable->find('all');
            $ward = $query5
                            ->select(['ward_id'])
                            ->where(['id' => $bookedexam['exam_id']])
                            ->toArray();

            /*debug($ward);
            debug($final_dailyturn[0]['id']);*/
            //updating la tupla del dailyturn
            $dailyturnsTable = TableRegistry::get('Dailyturns');
            $dailyturn = $dailyturnsTable->get($final_dailyturn[0]['id']);

            $dailyturn->ward_id = $ward[0]['ward_id'];
            $dailyturn->bookedexam_id = $bookedexam['id'];

            $dailyturnsTable->save($dailyturn); 

            return;
        } else {
            return;
        }
    }
    
    private function find_turn($day){
        $TurnsTable = TableRegistry::get('Turns');
        $query = $TurnsTable->find('all');
        $turn = $query
                ->select(['id'])
                ->where(['date' => $day])
                ->toArray();
        return $turn[0]['id'];
    }
    
    private function add_medicalrecord($data)
    {
        $uzerz = TableRegistry::get('Users');
        $query = $uzerz->find();
        $medicalrecord = $query->select(['medicalrecord_id'])->where(['id' =>$this->request->data['user_id']])->first();
        $original = $this->request->data; $original2 = $this->request->data;
        $result1 = array_slice($original,0,4); $result2 = array_slice($original2,4,1);
        $inserted = array("medicalrecord_id" => $medicalrecord['medicalrecord_id']);
        /*debug($this->request->data);
        debug($inserted);
        debug($result1); debug($result2);*/
        $result = array_merge($result1,$inserted,$result2);
        return $result;
    }
    
    private function check_date($data){
        $check = false;
        $start_date = date('Y-m-d');
        $request_date = $data["date"]["year"]."-".$data["date"]["month"]."-".$data["date"]["day"];
        $today_dt = strtotime($start_date);
        $request_dt = strtotime($request_date);
        if($request_dt > $today_dt){ //vuole salvare un'esame in un giorno prima di oggi
            $check = true;
        }
        return $check;
    }
    
    private function check_saves($data) {
        //non permette all'utente di salvare più volte lo stesso esame nella stessa data/ora
        //infatti non permette che l'utente salvi più di un'esame per data
        /*debug($data["date"]);
        debug($data["time"]);
        die;*/
        $date = $data["date"]["year"]."-".$data["date"]["month"]."-".$data["date"]["day"];
        $time = $data["time"]["hour"].":".$data["time"]["minute"];
        
        $books = TableRegistry::get('Bookedexams');
        $query = $books->find();
        $bookedexam = $query
                             ->select(['id'])
                             ->where(['date' => $date])
                             /*->where(['time' => $time])*/
                             /*->where(['exam_id' => $data["exam_id"]])*/ 
                             /*commentando questi significa che non può prenotare più di un'esame per data*/
                             ->where(['user_id' => $data["user_id"]])
                             ->first();
        //debug($bookedexam);
        if($bookedexam == null){
            return false;
        } else {
            return true;
        }
    }
    
    private function check_hours($data) //controlla quali orari sono disponibili
    {
        $date = $data["date"]["year"]."-".$data["date"]["month"]."-".$data["date"]["day"];
        $time = $data["time"]["hour"].":".$data["time"]["minute"];
        
        $books = TableRegistry::get('Bookedexams');
        $query = $books->find('all');
        $bookedexam = $query
                             ->select(['time'])
                             ->where(['date' => $date])
                             ->where(['exam_id' => $data["exam_id"]])
                             ->toArray();
        $exams = json_encode($bookedexam, JSON_PRETTY_PRINT);
        $examsArr = str_split($exams);
        $flag = 0;
        $pos = 0;
        $values = [];
        foreach ($examsArr as &$value) {
            $pos++;
            if($value == 'T'){
                array_push($values,substr($exams,$pos,5));
            }
        }
        //debug($values);
        
        //STEP1: quali orari non sono disponibili? $values
        $repeat = 0;
        $hours = ["09:00","10:00","11:00","12:00","14:00","15:00","16:00","17:00"];
        foreach ($hours as &$hour){
            $repeat = 0;
            foreach($values as &$value){
                if($hour == $value){
                    if($repeat){
                        //debug($hour);        
                        unset($hours[array_search($value,$hours)]);
                    }
                    $repeat++;
                }
            }
        }
        $hours = array_values($hours);
        //debug($hours);
        //debug($time);
        
        $res = [];
        array_push($res,false);
        $result = $res;
        foreach ($hours as &$hour){
            if($time == $hour){
                $result = array_replace($res, [true]);
            }
        }
        if($result[0] == false){
            array_push($result,$hours);
        }
        return $result;
    }
}
