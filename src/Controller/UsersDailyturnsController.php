<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersDailyturns Controller
 *
 * @property \App\Model\Table\UsersDailyturnsTable $UsersDailyturns
 */
class UsersDailyturnsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Dailyturns']
        ];
        $this->set('usersDailyturns', $this->paginate($this->UsersDailyturns));
        $this->set('_serialize', ['usersDailyturns']);
    }

    /**
     * View method
     *
     * @param string|null $id Users Dailyturn id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersDailyturn = $this->UsersDailyturns->get($id, [
            'contain' => ['Users', 'Dailyturns']
        ]);
        $this->set('usersDailyturn', $usersDailyturn);
        $this->set('_serialize', ['usersDailyturn']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersDailyturn = $this->UsersDailyturns->newEntity();
        if ($this->request->is('post')) {
            $usersDailyturn = $this->UsersDailyturns->patchEntity($usersDailyturn, $this->request->data);
            if ($this->UsersDailyturns->save($usersDailyturn)) {
                $this->Flash->success(__('The users dailyturn has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users dailyturn could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersDailyturns->Users->find('list', ['limit' => 200]);
        $dailyturns = $this->UsersDailyturns->Dailyturns->find('list', ['limit' => 200]);
        $this->set(compact('usersDailyturn', 'users', 'dailyturns'));
        $this->set('_serialize', ['usersDailyturn']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Dailyturn id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersDailyturn = $this->UsersDailyturns->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersDailyturn = $this->UsersDailyturns->patchEntity($usersDailyturn, $this->request->data);
            if ($this->UsersDailyturns->save($usersDailyturn)) {
                $this->Flash->success(__('The users dailyturn has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The users dailyturn could not be saved. Please, try again.'));
            }
        }
        $users = $this->UsersDailyturns->Users->find('list', ['limit' => 200]);
        $dailyturns = $this->UsersDailyturns->Dailyturns->find('list', ['limit' => 200]);
        $this->set(compact('usersDailyturn', 'users', 'dailyturns'));
        $this->set('_serialize', ['usersDailyturn']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Dailyturn id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersDailyturn = $this->UsersDailyturns->get($id);
        if ($this->UsersDailyturns->delete($usersDailyturn)) {
            $this->Flash->success(__('The users dailyturn has been deleted.'));
        } else {
            $this->Flash->error(__('The users dailyturn could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role == 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role == 1 || $role == 2){ //EMPLOYEER AND CLIENT AUTHORIZATION
            
            if (in_array($this->request->action, ['view', 'index', 'add', 'edit', 'delete'])) {
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Users', 'action' => 'view',$user['id']]); 
            } else {
                $this->Flash->error(__('Error in role Authorization process'));
            }
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
}
