<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Exams Controller
 *
 * @property \App\Model\Table\ExamsTable $Exams
 */
class ExamsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Wards']
        ];
        $this->set('exams', $this->paginate($this->Exams));
        $this->set('_serialize', ['exams']);
        
        /*Choose the right index */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName); /*$this->ProcessExamsView($methodName);*/
        /* */
    }

    /**
     * View method
     *
     * @param string|null $id Exam id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $exam = $this->Exams->get($id, [
            'contain' => ['Wards', 'Users', 'Bookedexams']
        ]);
        $this->set('exam', $exam);
        $this->set('_serialize', ['exam']);
        
        /*Choose the right view */
        $methodName = __FUNCTION__;
        //Log::debug($methodName);
        $this->ProcessView($methodName); /*$this->ProcessExamsView($methodName);*/
        /* */
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function addAdmin()
    {
        $exam = $this->Exams->newEntity();
        if ($this->request->is('post')) {
            $exam = $this->Exams->patchEntity($exam, $this->request->data);
            if ($this->Exams->save($exam)) {
                $this->Flash->success(__('The exam has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The exam could not be saved. Please, try again.'));
            }
        }
        $wards = $this->Exams->Wards->find('list', ['limit' => 200]);
        $users = $this->Exams->Users->find('list', ['limit' => 200])->where(['type' => "Doctor"]); 
        $this->set(compact('exam', 'wards', 'users'));
        $this->set('_serialize', ['exam']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Exam id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editAdmin($id = null)
    {
        $exam = $this->Exams->get($id, [
            'contain' => ['Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $exam = $this->Exams->patchEntity($exam, $this->request->data);
            if ($this->Exams->save($exam)) {
                $this->Flash->success(__('The exam has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The exam could not be saved. Please, try again.'));
            }
        }
        $wards = $this->Exams->Wards->find('list', ['limit' => 200]);
        $users = $this->Exams->Users->find('list', ['limit' => 200])->where(['type' => "Doctor"]); 
        $this->set(compact('exam', 'wards', 'users'));
        $this->set('_serialize', ['exam']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Exam id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $exam = $this->Exams->get($id);
        if ($this->Exams->delete($exam)) {
            $this->Flash->success(__('The exam has been deleted.'));
        } else {
            $this->Flash->error(__('The exam could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user)
    {
        $role =  $this->Auth->user("role");
        if($role == 0){ //ADMIN AUTHORIZATION
            return parent::isAuthorized($user);
            
        } else if($role == 1 || $role == 2){ //EMPLOYEER AND CLIENT AUTHORIZATION
            
            if (in_array($this->request->action, ['view','index'])) {
                return true;
            } else if(in_array($this->request->action, ['addAdmin','editAdmin','delete'])){
                $this->Flash->error(__('You cannot access that location'));
                $this->redirect(['controller' => 'Exams', 'action' => 'index']);
            }
            
        } else{
            $this->Flash->error(__('Error in Authorization process'));
        }        
    }
    
    /* Funzioni per il routing basato sul ruolo dell'user

     * 
     * PERCHE' USAVO QUESTE INVECE DI QUELLE IN APP CONTROLLER
     *      */ 
    /******************************** ROUTING ****************************************/
    protected function ProcessExamsView($methodName){
        $role =  $this->Auth->user("role");
        
        if($role == 0){
            $this->render($methodName."_admin");
        } else if($role == 1){
            $this->ProcessExamsViewForEmployeer($methodName);
        } else if($role == 2){
            $this->render($methodName); //template senza suffisso sono del client
        } else{
            $this->Flash->error(__('Error in Process View'));
        }       
    }
        
    protected function ProcessExamsViewForEmployeer($methodName){
        $type = $this->Auth->user("type");
        //cambia le view in base a se sei admin, client, doctor, nurse, janitor
                
        if(strcmp($type,'Doctor') == 0 || strcmp($type,'doctor') == 0){
            $this->render($methodName."_employeer_doctor");
        } else if(strcmp($type,'Nurse') == 0 || strcmp($type,'nurse') == 0  ){
            $this->render($methodName."_employeer_nurse");
        } else if(strcmp($type,'Janitor') == 0 || strcmp($type,'janitor') == 0 ){
            $this->render($methodName."_employeer_janitor"); 
        } else{
            $this->Flash->error(__('Error in Process View For Employeer'));
        }       
    }
}

