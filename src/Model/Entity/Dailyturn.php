<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Dailyturn Entity.
 *
 * @property int $id
 * @property int $ward_id
 * @property \App\Model\Entity\Ward $ward
 * @property int $turn_id
 * @property \App\Model\Entity\Turn $turn
 * @property int $bookedexam_id
 * @property \Cake\I18n\Time $time
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Bookedexam[] $bookedexams
 * @property \App\Model\Entity\User[] $users
 */
class Dailyturn extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
