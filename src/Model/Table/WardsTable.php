<?php
namespace App\Model\Table;

use App\Model\Entity\Ward;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Wards Model
 *
 * @property \Cake\ORM\Association\HasMany $Dailyturns
 * @property \Cake\ORM\Association\HasMany $Exams
 * @property \Cake\ORM\Association\HasMany $Rooms
 * @property \Cake\ORM\Association\HasMany $Warehouses
 */
class WardsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('wards');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Dailyturns', [
            'foreignKey' => 'ward_id'
        ]);
        $this->hasMany('Exams', [
            'foreignKey' => 'ward_id'
        ]);
        $this->hasMany('Rooms', [
            'foreignKey' => 'ward_id'
        ]);
        $this->hasMany('Warehouses', [
            'foreignKey' => 'ward_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }
}
