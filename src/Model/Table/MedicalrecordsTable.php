<?php
namespace App\Model\Table;

use App\Model\Entity\Medicalrecord;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Medicalrecords Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clients
 * @property \Cake\ORM\Association\HasMany $Bookedexams
 * @property \Cake\ORM\Association\HasMany $Treatments
 * @property \Cake\ORM\Association\HasMany $Users
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class MedicalrecordsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('medicalrecords');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Bookedexams', [
            'foreignKey' => 'medicalrecord_id'
        ]);
        $this->hasMany('Treatments', [
            'foreignKey' => 'medicalrecord_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'medicalrecord_id'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'medicalrecord_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'users_medicalrecords'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('client_name', 'create')
            ->notEmpty('client_name');

        $validator
            ->requirePresence('client_surname', 'create')
            ->notEmpty('client_surname');

        $validator
            ->requirePresence('client_ct', 'create')
            ->notEmpty('client_ct');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('anamnesis');

        $validator
            ->allowEmpty('medicalhistory');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->isUnique(['client_id']));
        $rules->add($rules->isUnique(['client_ct']));
        
        return $rules;
    }
}
