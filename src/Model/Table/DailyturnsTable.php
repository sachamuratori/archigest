<?php
namespace App\Model\Table;

use App\Model\Entity\Dailyturn;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dailyturns Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Wards
 * @property \Cake\ORM\Association\BelongsTo $Turns
 * @property \Cake\ORM\Association\BelongsTo $Bookedexams
 * @property \Cake\ORM\Association\HasMany $Bookedexams
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class DailyturnsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('dailyturns');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Wards', [
            'foreignKey' => 'ward_id'
        ]);
        $this->belongsTo('Turns', [
            'foreignKey' => 'turn_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Bookedexams', [
            'foreignKey' => 'bookedexam_id'
        ]);
        $this->hasMany('Bookedexams', [
            'foreignKey' => 'dailyturn_id'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'dailyturn_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'users_dailyturns'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('time', 'valid', ['rule' => 'time'])
            ->requirePresence('time', 'create')
            ->notEmpty('time');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ward_id'], 'Wards'));
        $rules->add($rules->existsIn(['turn_id'], 'Turns'));
        $rules->add($rules->existsIn(['bookedexam_id'], 'Bookedexams'));
        return $rules;
    }
}
