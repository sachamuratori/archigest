<?php
namespace App\Model\Table;

use App\Model\Entity\Warehouse;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Warehouses Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Wards
 * @property \Cake\ORM\Association\BelongsToMany $Articles
 */
class WarehousesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('warehouses');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Wards', [
            'foreignKey' => 'ward_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Articles', [
            'foreignKey' => 'warehouse_id',
            'targetForeignKey' => 'article_id',
            'joinTable' => 'warehouses_articles'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['ward_id'], 'Wards'));
        $rules->add($rules->isUnique(['ward_id']));
        return $rules;
    }
}
