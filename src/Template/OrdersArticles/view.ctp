<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <?php if($ordersArticle->order->evaded == "no"){ ?>
                <li><?= $this->Html->link(__('Edit Orders Article'), ['action' => 'edit', $ordersArticle->id]) ?> </li>
                <li><?= $this->Form->postLink(__('Delete Orders Article'), ['action' => 'delete', $ordersArticle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ordersArticle->id)]) ?> </li>
        <?php } ?>
        <li><?= $this->Html->link(__('Edit Orders Article'), ['action' => 'edit', $ordersArticle->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Orders Article'), ['action' => 'delete', $ordersArticle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ordersArticle->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Orders Articles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Orders Article'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ordersArticles view large-9 medium-8 columns content">
    <h3><?= h($ordersArticle->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Order') ?></th>
            <td><?= $ordersArticle->has('order') ? $this->Html->link($ordersArticle->order->name, ['controller' => 'Orders', 'action' => 'view', $ordersArticle->order->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Article') ?></th>
            <td><?= $ordersArticle->has('article') ? $this->Html->link($ordersArticle->article->name, ['controller' => 'Articles', 'action' => 'view', $ordersArticle->article->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Articlequantity') ?></th>
            <td><?= h($ordersArticle->articlequantity) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($ordersArticle->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Price') ?></th>
            <td><?= $this->Number->format($ordersArticle->price) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($ordersArticle->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($ordersArticle->modified) ?></tr>
        </tr>
    </table>
</div>
