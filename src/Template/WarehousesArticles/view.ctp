<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('Edit Warehouses Article'), ['action' => 'edit', $warehousesArticle->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Warehouses Article'), ['action' => 'delete', $warehousesArticle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehousesArticle->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Warehouses Articles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouses Article'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Warehouses'), ['controller' => 'Warehouses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Warehouse'), ['controller' => 'Warehouses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="warehousesArticles view large-9 medium-8 columns content">
    <h3><?= h($warehousesArticle->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Warehouse') ?></th>
            <td><?= $warehousesArticle->has('warehouse') ? $this->Html->link($warehousesArticle->warehouse->id, ['controller' => 'Warehouses', 'action' => 'view', $warehousesArticle->warehouse->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Article') ?></th>
            <td><?= $warehousesArticle->has('article') ? $this->Html->link($warehousesArticle->article->name, ['controller' => 'Articles', 'action' => 'view', $warehousesArticle->article->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Articlequantity') ?></th>
            <td><?= h($warehousesArticle->articlequantity) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($warehousesArticle->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($warehousesArticle->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($warehousesArticle->modified) ?></tr>
        </tr>
    </table>
</div>
