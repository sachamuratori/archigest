<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('New Warehouses Article'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Warehouses'), ['controller' => 'Warehouses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Warehouse'), ['controller' => 'Warehouses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="warehousesArticles index large-9 medium-8 columns content">
    <h3><?= __('Warehouses Articles') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('warehouse_id') ?></th>
                <th><?= $this->Paginator->sort('article_id') ?></th>
                <th><?= $this->Paginator->sort('articlequantity') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($warehousesArticles as $warehousesArticle): ?>
            <tr>
                <td><?= $this->Number->format($warehousesArticle->id) ?></td>
                <td><?= $warehousesArticle->has('warehouse') ? $this->Html->link($warehousesArticle->warehouse->id, ['controller' => 'Warehouses', 'action' => 'view', $warehousesArticle->warehouse->id]) : '' ?></td>
                <td><?= $warehousesArticle->has('article') ? $this->Html->link($warehousesArticle->article->name, ['controller' => 'Articles', 'action' => 'view', $warehousesArticle->article->id]) : '' ?></td>
                <td><?= h($warehousesArticle->articlequantity) ?></td>
                <td><?= h($warehousesArticle->created) ?></td>
                <td><?= h($warehousesArticle->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $warehousesArticle->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $warehousesArticle->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $warehousesArticle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $warehousesArticle->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
