<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('List Warehouses Articles'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Warehouses'), ['controller' => 'Warehouses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Warehouse'), ['controller' => 'Warehouses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="warehousesArticles form large-9 medium-8 columns content">
    <?= $this->Form->create($warehousesArticle) ?>
    <fieldset>
        <legend><?= __('Add Warehouses Article') ?></legend>
        <?php
            echo $this->Form->input('warehouse_id', ['options' => $warehouses]);
            echo $this->Form->input('article_id', ['options' => $articles]);
            echo $this->Form->input('articlequantity');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
