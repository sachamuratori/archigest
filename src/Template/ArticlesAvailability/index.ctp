<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('New Articles Availability'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="articlesAvailability index large-9 medium-8 columns content">
    <h3><?= __('Articles Availability') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('article_id') ?></th>
                <th><?= $this->Paginator->sort('availability') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($articlesAvailability as $articlesAvailability): ?>
            <tr>
                <td><?= $this->Number->format($articlesAvailability->id) ?></td>
                <td><?= $articlesAvailability->has('article') ? $this->Html->link($articlesAvailability->article->name, ['controller' => 'Articles', 'action' => 'view', $articlesAvailability->article->id]) : '' ?></td>
                <td><?= $this->Number->format($articlesAvailability->availability) ?></td>
                <td><?= h($articlesAvailability->created) ?></td>
                <td><?= h($articlesAvailability->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $articlesAvailability->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $articlesAvailability->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $articlesAvailability->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articlesAvailability->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
