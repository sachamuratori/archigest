<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('List Bookedexams'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Exam'), ['controller' => 'Exams', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Medicalrecord'), ['controller' => 'Medicalrecords', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dailyturn'), ['controller' => 'Dailyturns', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bookedexams form large-9 medium-8 columns content">
    <?= $this->Form->create($bookedexam) ?>
    <fieldset>
        <legend><?= __('Add Bookedexam') ?></legend>
        <?php
            echo $this->Form->input('exam_id', ['options' => $exams]);
            echo $this->Form->input('date');
            echo $this->Form->input('time', [
                                        'type' => 'time',
                                        'interval' => 60
                                    ]);
            echo $this->Form->label('(Hours available are between 09:00 -- 12:00 and 14:00 -- 17:00)');
            ?> <br> <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('result');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
