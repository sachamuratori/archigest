<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Client') ?></li>
        <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="bookedexams form large-9 medium-8 columns content">
    <?= $this->Form->create($bookedexam) ?>
    <fieldset>
        <legend><?= __('Add Bookedexam') ?></legend>
        <?php
            echo $this->Form->input('exam_id', ['options' => $exams]);
            echo $this->Form->input('date', [
                                        'minYear' => date('Y')
                                    ]);
            echo $this->Form->input('time', [
                                        'type' => 'time',
                                        'interval' => 60,     
                                    ]);
            echo $this->Form->label('(Hours available are between 09:00 -- 12:00 and 14:00 -- 17:00)');
            ?> <br> <?php
            echo $this->Form->input('user_id', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
