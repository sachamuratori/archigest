<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Nurse') ?></li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('List Treatments'), ['controller' => 'Treatments', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="medicalrecords view large-9 medium-8 columns content">
    <h3><?= h($medicalrecord->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($medicalrecord->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($medicalrecord->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Client Id') ?></th>
            <td><?= $this->Number->format($medicalrecord->client_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Client Name') ?></th>
            <td><?= h($medicalrecord->client_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Client Surname') ?></th>
            <td><?= h($medicalrecord->client_surname) ?></td>
        </tr>
        <tr>
            <th><?= __('Client Ct') ?></th>
            <td><?= h($medicalrecord->client_ct) ?></td>
        </tr>
        <tr>
            <th><?= __('Anamnesis') ?></th>
            <td><?= h($medicalrecord->anamnesis) ?></td>
        </tr>
        <tr>
            <th><?= __('Medicalhistory') ?></th>
            <td><?= h($medicalrecord->medicalhistory) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($medicalrecord->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($medicalrecord->modified) ?></tr>
        </tr>
    </table>
    
    <div class="related">
        <h4><?= __('Related Treatments') ?></h4>
        <?php if (!empty($medicalrecord->treatments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Medicalrecord Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Startdate') ?></th>
                <th><?= __('Enddate') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($medicalrecord->treatments as $treatments): ?>
            <tr>
                <td><?= h($treatments->id) ?></td>
                <td><?= h($treatments->medicalrecord_id) ?></td>
                <td><?= h($treatments->user_id) ?></td>
                <td><?= $this->Html->link(__($treatments->name), ['controller' => 'Treatments', 'action' => 'view', $treatments->id]) ?></td>
                <td><?= h($treatments->description) ?></td>
                <td><?= h($treatments->startdate) ?></td>
                <td><?= h($treatments->enddate) ?></td>
                <td><?= h($treatments->created) ?></td>
                <td><?= h($treatments->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Treatments', 'action' => 'view', $treatments->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Bookedexams') ?></h4>
        <?php if (!empty($medicalrecord->bookedexams)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Exam Id') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Time') ?></th>
                <th><?= __('Result') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($medicalrecord->bookedexams as $bookedexams): ?>
            <tr>
                <td><?= h($bookedexams->id) ?></td>
                <td><?= h($bookedexams->exam_id) ?></td>
                <td><?= h($bookedexams->date) ?></td>
                <td><?= h($bookedexams->time) ?></td>
                <td><?= h($bookedexams->result) ?></td>
                <td><?= h($bookedexams->created) ?></td>
                <td><?= h($bookedexams->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bookedexams', 'action' => 'view', $bookedexams->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
