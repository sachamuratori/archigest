<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Nurse') ?></li>
        <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Treatments'), ['controller' => 'Treatments', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="medicalrecords index large-9 medium-8 columns content">
    <h3><?= __('Medicalrecords') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('client_id') ?></th>
                <th><?= $this->Paginator->sort('client_name') ?></th>
                <th><?= $this->Paginator->sort('client_surname') ?></th>
                <th><?= $this->Paginator->sort('client_ct') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($medicalrecords as $medicalrecord): ?>
            <tr>
                <td><?= $this->Number->format($medicalrecord->id) ?></td>
                <td><?= $this->Number->format($medicalrecord->client_id) ?></td>
                <td><?= h($medicalrecord->client_name) ?></td>
                <td><?= h($medicalrecord->client_surname) ?></td>
                <td><?= h($medicalrecord->client_ct) ?></td>
                <td><?= h($medicalrecord->created) ?></td>
                <td><?= h($medicalrecord->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $medicalrecord->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
