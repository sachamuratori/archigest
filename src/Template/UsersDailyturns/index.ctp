<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Users Dailyturn'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dailyturn'), ['controller' => 'Dailyturns', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersDailyturns index large-9 medium-8 columns content">
    <h3><?= __('Users Dailyturns') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('dailyturn_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersDailyturns as $usersDailyturn): ?>
            <tr>
                <td><?= $this->Number->format($usersDailyturn->id) ?></td>
                <td><?= $usersDailyturn->has('user') ? $this->Html->link($usersDailyturn->user->name, ['controller' => 'Users', 'action' => 'view', $usersDailyturn->user->id]) : '' ?></td>
                <td><?= $usersDailyturn->has('dailyturn') ? $this->Html->link($usersDailyturn->dailyturn->id, ['controller' => 'Dailyturns', 'action' => 'view', $usersDailyturn->dailyturn->id]) : '' ?></td>
                <td><?= h($usersDailyturn->created) ?></td>
                <td><?= h($usersDailyturn->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usersDailyturn->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usersDailyturn->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usersDailyturn->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersDailyturn->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
