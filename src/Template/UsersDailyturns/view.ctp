<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Users Dailyturn'), ['action' => 'edit', $usersDailyturn->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Dailyturn'), ['action' => 'delete', $usersDailyturn->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersDailyturn->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Dailyturns'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Dailyturn'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dailyturn'), ['controller' => 'Dailyturns', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersDailyturns view large-9 medium-8 columns content">
    <h3><?= h($usersDailyturn->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $usersDailyturn->has('user') ? $this->Html->link($usersDailyturn->user->name, ['controller' => 'Users', 'action' => 'view', $usersDailyturn->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Dailyturn') ?></th>
            <td><?= $usersDailyturn->has('dailyturn') ? $this->Html->link($usersDailyturn->dailyturn->id, ['controller' => 'Dailyturns', 'action' => 'view', $usersDailyturn->dailyturn->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($usersDailyturn->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($usersDailyturn->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($usersDailyturn->modified) ?></tr>
        </tr>
    </table>
</div>
