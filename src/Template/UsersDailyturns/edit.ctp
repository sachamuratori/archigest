<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $usersDailyturn->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $usersDailyturn->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Users Dailyturns'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dailyturn'), ['controller' => 'Dailyturns', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersDailyturns form large-9 medium-8 columns content">
    <?= $this->Form->create($usersDailyturn) ?>
    <fieldset>
        <legend><?= __('Edit Users Dailyturn') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('dailyturn_id', ['options' => $dailyturns]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
