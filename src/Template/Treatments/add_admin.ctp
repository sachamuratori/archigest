<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('List Treatments'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Medicalrecord'), ['controller' => 'Medicalrecords', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="treatments form large-9 medium-8 columns content">
    <?= $this->Form->create($treatment) ?>
    <fieldset>
        <legend><?= __('Add Treatment') ?></legend>
        <?php
            echo $this->Form->input('medicalrecord_id', ['options' => $medicalrecords]);
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('name');
            echo $this->Form->input('description');
            echo $this->Form->input('startdate', [
                                        'type' => 'datetime',
                                        'interval' => 60
                                    ]);
            echo $this->Form->input('enddate', [
                                        'type' => 'datetime',
                                        'interval' => 60
                                    ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
