<?php
$this->element('sidebars');
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
         <?php echo $this->fetch('sidebar_employeer_doctor'); ?>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name), ' ', h($user->surname) ?></h3>
        
    <div class="information_general">
        <table class="vertical-table">
            <h5><?= __('General Information') ?></h5>
            <tr>
                <th><?= __('Id') ?></th>
                <td><?= $this->Number->format($user->id) ?></td>
            </tr>
            <tr>
                <th><?= __('Role') ?></th>
                <td><?php 
                        if(($user->role) === 0){
                            echo 'Admin';
                        } else if(($user->role) === 1) {
                            echo 'Employeer';
                        } else if(($user->role) === 2) {
                            echo 'Client';
                        }
                    ?></td>
            </tr>
            <tr>
                <th><?= __('Email') ?></th>
                <td><?= h($user->email) ?></td>
            </tr>
            <tr>
                <th><?= __('Password') ?></th>
                <td><?= h($user->password) ?></td>
            </tr>
        </table>
    </div>
    
    <div class="information_identity">
        <table class="vertical-table">
            <h5><?= __('Identity Information') ?></h5>
            <tr>
                <th><?= __('Name') ?></th>
                <td><?= h($user->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Surname') ?></th>
                <td><?= h($user->surname) ?></td>
            </tr>
            <tr>
                <th><?= __('Dateofbirth') ?></th>
                <td><?= h($user->dateofbirth->format('Y-m-d')) ?></td>
            </tr>
            <tr>
                <th><?= __('Sex') ?></th>
                <td><?php 
                        if(($user->sex) == 0){
                            echo 'M';
                        } else if(($user->sex) == 1) {
                            echo 'F';
                        } else if(($user->sex) == 2) {
                            echo 'Not Specified';
                        }
                ?></td>
            </tr>
            <tr>
                <th><?= __('Cf') ?></th>
                <td><?= h($user->cf) ?></td>
            </tr>
            <tr>
                <th><?= __('Address') ?></th>
                <td><?= h($user->address) ?></td>
            </tr>
            <tr>
                <th><?= __('City') ?></th>
                <td><?= h($user->city) ?></td>
            </tr>
            <tr>
                <th><?= __('Postalcode') ?></th>
                <td><?= h($user->postalcode) ?></td>
            </tr>
            <tr>
                <th><?= __('Province') ?></th>
                <td><?= h($user->province) ?></td>
            </tr>
            <tr>
                <th><?= __('Country') ?></th>
                <td><?= h($user->country) ?></td>
            </tr>
            <tr>
                <th><?= __('Telephone1') ?></th>
                <td><?= h($user->telephone1) ?></td>
            </tr>
            <tr>
                <th><?= __('Telephone2') ?></th>
                <td><?= h($user->telephone2) ?></td>
            </tr>
            <tr>
                <th><?= __('Ct') ?></th>
                <td><?= h($user->ct) ?></td>
            </tr>
        </table>
    </div>

    <div class="information_employeer">
        <table class="vertical-table">
            <h5><?= __('Employeer Information') ?></h5>
            <tr>
                <th><?= __('Type') ?></th>
                <td><?= h($user->type) ?></td>
            </tr>
            <tr>
                <th><?= __('Specialization') ?></th>
                <td><?= h($user->specialization) ?></td>
            </tr>
            <tr>
                <th><?= __('Medicalrecord_id') ?></th>
                <td><?php 
                        if(($user->medicalrecord_id) === 0){
                            echo '';
                        } else {
                            echo ($user->medicalrecord_id);
                        }
                    ?></td>
            </tr>
            <tr>
                <th><?= __('Salary') ?></th>
                <td><?= $this->Number->format($user->salary) ?></td>
            </tr>
            <tr>
                <th><?= __('Recruited by') ?></th>
                <td><?= 'Admin ', h($adminName), ' ',h($adminSurname) ?></td>
            </tr>
        </table>
    </div>
      
    <div class="information_database">
        <table class="vertical-table">
            <tr>
                <th><?= __('Created') ?></th>
                <td><?= h($user->created) ?></tr>
            </tr>
            <tr>
                <th><?= __('Modified') ?></th>
                <td><?= h($user->modified) ?></tr>
            </tr>
        </table>
    </div>
    
    <div class="related">
        <h4><?= __('Related Exams') ?></h4>
        <?php if (!empty($user->exams)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Ward Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->exams as $exams): ?>
            <tr>
                <td><?= h($exams->id) ?></td>
                <td><?= h($exams->ward_id) ?></td>
                <td><?= h($exams->name) ?></td>
                <td><?= h($exams->description) ?></td>
                <td><?= h($exams->created) ?></td>
                <td><?= h($exams->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Exams', 'action' => 'view', $exams->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Exams', 'action' => 'edit', $exams->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Exams', 'action' => 'delete', $exams->id], ['confirm' => __('Are you sure you want to delete # {0}?', $exams->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    
    <div class="related">
        <h4><?= __('Related Medicalrecords') ?></h4>
        <?php if (!empty($user->medicalrecords)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Anamnesis') ?></th>
                <th><?= __('Medicalhistory') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->medicalrecords as $medicalrecords): ?>
            <tr>
                <td><?= h($medicalrecords->id) ?></td>
                <td><?= h($medicalrecords->name) ?></td>
                <td><?= h($medicalrecords->anamnesis) ?></td>
                <td><?= h($medicalrecords->medicalhistory) ?></td>
                <td><?= h($medicalrecords->created) ?></td>
                <td><?= h($medicalrecords->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Medicalrecords', 'action' => 'view', $medicalrecords->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Medicalrecords', 'action' => 'edit', $medicalrecords->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Medicalrecords', 'action' => 'delete', $medicalrecords->id], ['confirm' => __('Are you sure you want to delete # {0}?', $medicalrecords->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    
    <div class="related">
        <h4><?= __('Related Treatments') ?></h4>
        <?php if (!empty($user->treatments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Medicalrecord Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Startdate') ?></th>
                <th><?= __('Enddate') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->treatments as $treatments): ?>
            <tr>
                <td><?= h($treatments->id) ?></td>
                <td><?= h($treatments->medicalrecord_id) ?></td>
                <td><?= h($treatments->user_id) ?></td>
                <td><?= h($treatments->name) ?></td>
                <td><?= h($treatments->description) ?></td>
                <td><?= h($treatments->startdate) ?></td>
                <td><?= h($treatments->enddate) ?></td>
                <td><?= h($treatments->created) ?></td>
                <td><?= h($treatments->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Treatments', 'action' => 'view', $treatments->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Treatments', 'action' => 'edit', $treatments->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Treatments', 'action' => 'delete', $treatments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $treatments->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    
    <div class="related">
        <h4><?= __('Related Dailyturns') ?></h4>
        <?php if (!empty($user->dailyturns)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Ward Id') ?></th>
                <th><?= __('Turn Id') ?></th>
                <th><?= __('Bookedexam Id') ?></th>
                <th><?= __('Time') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->dailyturns as $dailyturns): ?>
            <tr>
                <td><?= h($dailyturns->id) ?></td>
                <td><?= h($dailyturns->ward_id) ?></td>
                <td><?= h($dailyturns->turn_id) ?></td>
                <td><?= h($dailyturns->bookedexam_id) ?></td>
                <td><?= h($dailyturns->time->format('H:i')) ?></td>
                <td><?= h($dailyturns->created) ?></td>
                <td><?= h($dailyturns->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Dailyturns', 'action' => 'view', $dailyturns->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
