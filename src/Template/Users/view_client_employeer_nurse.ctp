<?php
$this->element('sidebars');
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
         <?php echo $this->fetch('sidebar_employeer_nurse'); ?>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name), ' ', h($user->surname) ?></h3>
        
    <div class="information_general">
        <table class="vertical-table">
            <h5><?= __('General Information') ?></h5>
            <tr>
                <th><?= __('Id') ?></th>
                <td><?= $this->Number->format($user->id) ?></td>
            </tr>
            <tr>
                <th><?= __('Role') ?></th>
                <td><?php 
                        if(($user->role) === 0){
                            echo 'Admin';
                        } else if(($user->role) === 1) {
                            echo 'Employeer';
                        } else if(($user->role) === 2) {
                            echo 'Client';
                        }
                    ?></td>
            </tr>
            <tr>
                <th><?= __('Email') ?></th>
                <td><?= h($user->email) ?></td>
            </tr>
            <tr>
                <th><?= __('Password') ?></th>
                <td><?= h($user->password) ?></td>
            </tr>
        </table>
    </div>
    
    <div class="information_identity">
        <table class="vertical-table">
            <h5><?= __('Identity Information') ?></h5>
            <tr>
                <th><?= __('Name') ?></th>
                <td><?= h($user->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Surname') ?></th>
                <td><?= h($user->surname) ?></td>
            </tr>
            <tr>
                <th><?= __('Dateofbirth') ?></th>
                <td><?= h($user->dateofbirth->format('Y-m-d')) ?></td>
            </tr>
            <tr>
                <th><?= __('Sex') ?></th>
                <td><?= h($user->sex) ?></td>
            </tr>
            <tr>
                <th><?= __('Cf') ?></th>
                <td><?= h($user->cf) ?></td>
            </tr>
            <tr>
                <th><?= __('Address') ?></th>
                <td><?= h($user->address) ?></td>
            </tr>
            <tr>
                <th><?= __('City') ?></th>
                <td><?= h($user->city) ?></td>
            </tr>
            <tr>
                <th><?= __('Postalcode') ?></th>
                <td><?= h($user->postalcode) ?></td>
            </tr>
            <tr>
                <th><?= __('Province') ?></th>
                <td><?= h($user->province) ?></td>
            </tr>
            <tr>
                <th><?= __('Country') ?></th>
                <td><?= h($user->country) ?></td>
            </tr>
            <tr>
                <th><?= __('Telephone1') ?></th>
                <td><?= h($user->telephone1) ?></td>
            </tr>
            <tr>
                <th><?= __('Telephone2') ?></th>
                <td><?= h($user->telephone2) ?></td>
            </tr>
            <tr>
                <th><?= __('Ct') ?></th>
                <td><?= h($user->ct) ?></td>
            </tr>
        </table>
    </div>

    <div class="information_database">
        <table class="vertical-table">
            <tr>
                <th><?= __('Created') ?></th>
                <td><?= h($user->created) ?></tr>
            </tr>
            <tr>
                <th><?= __('Modified') ?></th>
                <td><?= h($user->modified) ?></tr>
            </tr>
        </table>
    </div>
    
    <div class="related">
        <h4><?= __('Related Bookedexams') ?></h4>
        <?php if (!empty($user->bookedexams)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Exam Id') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Time') ?></th>
                <th><?= __('Result') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->bookedexams as $bookedexams): ?>
            <tr>
                <td><?= h($bookedexams->id) ?></td>
                <td><?= h($bookedexams->exam_id) ?></td>
                <td><?= h($bookedexams->date->format('Y-m-d')) ?></td>
                <td><?= h($bookedexams->time->format('H:i')) ?></td>
                <td><?= h($bookedexams->result) ?></td>
                <td><?= h($bookedexams->created) ?></td>
                <td><?= h($bookedexams->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bookedexams', 'action' => 'view', $bookedexams->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
  
</div>
