<div class="clients form large-10 medium-9 columns">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Welcome to Registration, please fulfill all fields') ?></legend>
        <?php
            //echo $this->Form->input('role'); //devo metterlo io automaticamente a client o '2'
            //admin 0, employer 1, client 2
            echo $this->Form->input('email');
            echo $this->Form->input('password');
            echo $this->Form->input('name');
            echo $this->Form->input('surname');
            echo $this->Form->input('sex', array(
                'options' => array("M", "F", "Not Specified")
                ));
            echo $this->Form->input('dateofbirth', array(
                    'label' => 'Date of birth',
                    'dateFormat' => 'DMY',
                    'minYear' => date('Y') - 80,
                    'maxYear' => date('Y') - 14,
                ));
            echo $this->Form->input('cf');
            echo $this->Form->input('address');
            echo $this->Form->input('city');
            echo $this->Form->input('postalcode');
            echo $this->Form->input('province');
            echo $this->Form->input('country');
            echo $this->Form->input('telephone1');
            echo $this->Form->input('telephone2');
            // echo $this->Form->input('medicalrecord_id'); questo??? una volta che si crea la cartella clinica si
            // DEVE AGGIORNARE?
            echo $this->Form->input('ct', array(
                'required' => true
                ));
            /*echo $this->Form->input('type');
            echo $this->Form->input('salary');
            echo $this->Form->input('specialization');
            echo $this->Form->input('user_id');*/ //valori che devono essere null
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
