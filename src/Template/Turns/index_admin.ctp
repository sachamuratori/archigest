<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('New Turn'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dailyturn'), ['controller' => 'Dailyturns', 'action' => 'add']) ?></li>
        <br>
        <li><?= $this->Html->link(__('Automatic Turn Generator'), ['action' => 'atg']) ?></li>
        <br>
    </ul>
</nav>
<div class="turns index large-9 medium-8 columns content">
    <h3><?= __('Turns') ?></h3>
    <li><?= $this->Html->link(__('Generate Automatic Turns'), ['controller' => 'Turns', 'action' => 'atg']) ?></li>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('date') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($turns as $turn): ?>
            <tr>
                <td><?= $this->Number->format($turn->id) ?></td>
                <td><?= h($turn->date->format('Y-m-d')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $turn->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $turn->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $turn->id], ['confirm' => __('Are you sure you want to delete # {0}?', $turn->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
