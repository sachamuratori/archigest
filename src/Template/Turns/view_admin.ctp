<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('Edit Turn'), ['action' => 'edit', $turn->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Turn'), ['action' => 'delete', $turn->id], ['confirm' => __('Are you sure you want to delete # {0}?', $turn->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Turns'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Turn'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dailyturn'), ['controller' => 'Dailyturns', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="turns view large-9 medium-8 columns content">
    <h3><?= h($turn->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($turn->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date') ?></th>
            <td><?= h($turn->date->format('Y-m-d')) ?></tr>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($turn->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($turn->modified) ?></tr>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Dailyturns') ?></h4>
        <?php if (!empty($turn->dailyturns)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Ward Id') ?></th>
                <th><?= __('Turn Id') ?></th>
                <th><?= __('Bookedexam Id') ?></th>
                <th><?= __('Time') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($turn->dailyturns as $dailyturns): ?>
            <tr>
                <td><?= h($dailyturns->id) ?></td>
                <td><?= h($dailyturns->ward_id) ?></td>
                <td><?= h($dailyturns->turn_id) ?></td>
                <td><?= h($dailyturns->bookedexam_id) ?></td>
                <td><?= h($dailyturns->time) ?></td>
                <td><?= h($dailyturns->created) ?></td>
                <td><?= h($dailyturns->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Dailyturns', 'action' => 'view', $dailyturns->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Dailyturns', 'action' => 'edit', $dailyturns->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dailyturns', 'action' => 'delete', $dailyturns->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dailyturns->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
