<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Doctor') ?></li>
        <li><?= $this->Html->link(__('List Wards'), ['controller' => 'Wards', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Turns'), ['controller' => 'Turns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="dailyturns index large-9 medium-8 columns content">
    <h3><?= __('Dailyturns') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('ward_id') ?></th>
                <th><?= $this->Paginator->sort('turn_id') ?></th>
                <th><?= $this->Paginator->sort('time') ?></th>
                <th><?= $this->Paginator->sort('bookedexam_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dailyturns as $dailyturn): ?>
            <tr>
               <td><?= $this->Number->format($dailyturn->id) ?></td>
                <td><?= $dailyturn->has('ward') ? $this->Html->link($dailyturn->ward->name, ['controller' => 'Wards', 'action' => 'view', $dailyturn->ward->id]) : '' ?></td>
                <td><?= $dailyturn->has('turn') ? $this->Html->link($dailyturn->turn->date->format('Y-m-d'), ['controller' => 'Turns', 'action' => 'view', $dailyturn->turn->id]) : '' ?></td>
                <td><?= h($dailyturn->time->format('H:i')) ?></td>
                <td><?= $this->Html->link($dailyturn->bookedexam_id, ['controller' => 'Bookedexams', 'action' => 'view', $dailyturn->bookedexam_id]) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dailyturn->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
