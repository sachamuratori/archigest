<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Wards'), ['controller' => 'Wards', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ward'), ['controller' => 'Wards', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Turns'), ['controller' => 'Turns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Turn'), ['controller' => 'Turns', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bookedexam'), ['controller' => 'Bookedexams', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dailyturns form large-9 medium-8 columns content">
    <?= $this->Form->create($dailyturn) ?>
    <fieldset>
        <legend><?= __('Add Dailyturn') ?></legend>
        <?php
            echo $this->Form->input('ward_id', ['options' => $wards, 'empty' => true]);
            echo $this->Form->input('turn_id', ['options' => $turns]);
            echo $this->Form->input('bookedexam',['options' => $bookedexams]);
            echo $this->Form->input('time', [
                                        'type' => 'time',
                                        'interval' => 60
                                    ]);
            echo $this->Form->label('(hours available are between 09:00 -- 12:00 and 14:00 -- 17:00)');
            ?> <br> <?php
            echo $this->Form->input('users._ids'.' - employeer', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
