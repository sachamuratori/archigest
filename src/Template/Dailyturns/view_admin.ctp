<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('Edit Dailyturn'), ['action' => 'edit', $dailyturn->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Dailyturn'), ['action' => 'delete', $dailyturn->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dailyturn->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Dailyturns'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dailyturn'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wards'), ['controller' => 'Wards', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ward'), ['controller' => 'Wards', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Turns'), ['controller' => 'Turns', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Turn'), ['controller' => 'Turns', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bookedexam'), ['controller' => 'Bookedexams', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dailyturns view large-9 medium-8 columns content">
    <h3><?= h($dailyturn->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($dailyturn->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Turn') ?></th>
            <td><?= $dailyturn->has('turn') ? $this->Html->link($dailyturn->turn->date->format('Y-m-d'), ['controller' => 'Turns', 'action' => 'view', $dailyturn->turn->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Time') ?></th>
            <td><?= h($dailyturn->time->format('H:i')) ?></tr>
        </tr>
        <tr>
            <th><?= __('Ward') ?></th>
            <td><?= $dailyturn->has('ward') ? $this->Html->link($dailyturn->ward->name, ['controller' => 'Wards', 'action' => 'view', $dailyturn->ward->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Bookedexam Id') ?></th>
            <td><?= $this->Number->format($dailyturn->bookedexam_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($dailyturn->created) ?></tr>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($dailyturn->modified) ?></tr>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Bookedexams') ?></h4>
        <?php if (!empty($dailyturn->bookedexams)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Exam Id') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Time') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Medicalrecord Id') ?></th>
                <th><?= __('Dailyturn Id') ?></th>
                <th><?= __('Result') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dailyturn->bookedexams as $bookedexams): ?>
            <tr>
                <td><?= h($bookedexams->id) ?></td>
                <td><?= h($bookedexams->exam_id) ?></td>
                <td><?= h($bookedexams->date) ?></td>
                <td><?= h($bookedexams->time) ?></td>
                <td><?= h($bookedexams->user_id) ?></td>
                <td><?= h($bookedexams->medicalrecord_id) ?></td>
                <td><?= h($bookedexams->dailyturn_id) ?></td>
                <td><?= h($bookedexams->result) ?></td>
                <td><?= h($bookedexams->created) ?></td>
                <td><?= h($bookedexams->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bookedexams', 'action' => 'view', $bookedexams->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bookedexams', 'action' => 'edit', $bookedexams->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bookedexams', 'action' => 'delete', $bookedexams->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookedexams->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($dailyturn->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Role') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Surname') ?></th>
                <th><?= __('Type') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dailyturn->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->role) ?></td>
                <td><?= h($users->name) ?></td>
                <td><?= h($users->surname) ?></td>
                <td><?= h($users->type) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
