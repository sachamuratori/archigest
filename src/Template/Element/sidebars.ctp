<?php /************************ SIDEBAR ADMIN *******************/ ?>
<?php $this->start('sidebar_admin'); ?>
<li class="heading"><?= __('Welcome Admin') ?></li>
<div class="sidebar-pagewithsidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse-pagewithsidebar">
        <ul class="nav ul-pageswithsidebar" id="side-menu">
            <li class="heading"><?= __('Users') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('List User'), ['action' => 'indexAdmin']) ?></li>
                    <li><?= $this->Html->link(__('New User'), ['action' => 'addAdmin']) ?></li>
                    <li><?= $this->Html->link(__('Set Recruited'), ['action' => 'set_recruited']) ?></li>
            </ul>
            <li class="heading"><?= __('Managment') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Orders Articles'), ['controller' => 'OrdersArticles', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Order Articles'), ['controller' => 'OrdersArticles', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Articles Availability'), ['controller' => 'ArticlesAvailability', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Suppliers'), ['controller' => 'Suppliers', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Supplier'), ['controller' => 'Suppliers', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Warehouses'), ['controller' => 'Warehouses', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Warehouse'), ['controller' => 'Warehouses', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Wards'), ['controller' => 'Wards', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Ward'), ['controller' => 'Wards', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Warehouses Articles'), ['controller' => 'WarehousesArticles', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Warehouse Article'), ['controller' => 'WarehousesArticles', 'action' => 'add']) ?></li>
            </ul>
            <li class="heading"><?= __('Employeer') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Dailyturn'), ['controller' => 'Dailyturns', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Turns'), ['controller' => 'Turns', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Turn'), ['controller' => 'Turns', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Users Medicalrecords'), ['controller' => 'UsersMedicalrecords', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Users Medicalrecords'), ['controller' => 'UsersMedicalrecords', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Users Exams'), ['controller' => 'UsersExams', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Users Exams'), ['controller' => 'UsersExams', 'action' => 'add']) ?></li>
            </ul>
            <li class="heading"><?= __('Client') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Exam'), ['controller' => 'Exams', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Bookedexam'), ['controller' => 'Bookedexams', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Medicalrecord'), ['controller' => 'Medicalrecords', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Treatments'), ['controller' => 'Treatments', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Treatment'), ['controller' => 'Treatments', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Room'), ['controller' => 'Rooms', 'action' => 'add']) ?></li>
            </ul>
        </ul>
    </div>
</div>
<!-- /.sidebar-collapse -->
<!-- /.navbar-static-side -->
<?php $this->end(); ?>

<?php /************************ SIDEBAR DOCTOR *******************/ ?>
<?php $this->start('sidebar_employeer_doctor'); ?>
<li class="heading"><?= __('Welcome Doctor') ?></li>
<div class="sidebar-pagewithsidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse-pagewithsidebar">
        <ul class="nav ul-pageswithsidebar" id="side-menu">
            <li class="heading"><?= __('User') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('Edit User'), ['controller' => 'Users', 'action' => 'edit',$user['id']]) ?></li>
            </ul>
            <li class="heading"><?= __('Employeer') ?></li>
            <ul class="nav nav-second-level">
                <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Turns'), ['controller' => 'Turns', 'action' => 'index']) ?></li>
            </ul>
            <li class="heading"><?= __('Client') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Medicalrecord'), ['controller' => 'Medicalrecords', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Treatments'), ['controller' => 'Treatments', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Treatment'), ['controller' => 'Treatments', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Wards'), ['controller' => 'Wards', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Warehouses'), ['controller' => 'Warehouses', 'action' => 'index']) ?></li>
            </ul>
        </ul>
    </div>
</div>
<?php $this->end(); ?>

<?php /************************ SIDEBAR NURSE *******************/ ?>
<?php $this->start('sidebar_employeer_nurse'); ?>
<li class="heading"><?= __('Welcome Nurse') ?></li>
<div class="sidebar-pagewithsidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse-pagewithsidebar">
        <ul class="nav ul-pageswithsidebar" id="side-menu">
            <li class="heading"><?= __('User') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('Edit User'), ['controller' => 'Users', 'action' => 'edit',$user['id']]) ?></li>
            </ul>
            <li class="heading"><?= __('Employeer') ?></li>
            <ul class="nav nav-second-level">
                <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Turns'), ['controller' => 'Turns', 'action' => 'index']) ?></li>
            </ul>
            <li class="heading"><?= __('Client') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?></li>    
                    <li><?= $this->Html->link(__('List Treatments'), ['controller' => 'Treatments', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Wards'), ['controller' => 'Wards', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Warehouses'), ['controller' => 'Warehouses', 'action' => 'index']) ?></li>
            </ul>
        </ul>
    </div>
</div>
<?php $this->end(); ?>

<?php /************************ SIDEBAR JANITOR *******************/ ?>
<?php $this->start('sidebar_employeer_janitor'); ?>
<li class="heading"><?= __('Welcome Janitor') ?></li>
<div class="sidebar-pagewithsidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse-pagewithsidebar">
        <ul class="nav ul-pageswithsidebar" id="side-menu">
            <li class="heading"><?= __('User') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('Edit User'), ['controller' => 'Users', 'action' => 'edit',$user['id']]) ?></li>
            </ul>
            <li class="heading"><?= __('Employeer') ?></li>
            <ul class="nav nav-second-level">
                <li><?= $this->Html->link(__('List Dailyturns'), ['controller' => 'Dailyturns', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Turns'), ['controller' => 'Turns', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Wards'), ['controller' => 'Wards', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Warehouses'), ['controller' => 'Warehouses', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Bookedexams'), ['controller' => 'Bookedexams', 'action' => 'index']) ?></li>
            </ul>
        </ul>
    </div>
</div>
<?php $this->end(); ?>

<?php /************************ SIDEBAR CLIENT *******************/ ?>
<?php $this->start('sidebar_client'); ?>
<li class="heading"><?= __('Welcome Client') ?></li>
<div class="sidebar-pagewithsidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse-pagewithsidebar">
        <ul class="nav ul-pageswithsidebar" id="side-menu">
            <li class="heading"><?= __('User') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('Edit User'), ['controller' => 'Users', 'action' => 'edit',$user['id']]) ?></li>
            </ul>
            <li class="heading"><?= __('Client') ?></li>
            <ul class="nav nav-second-level">
                    <li><?= $this->Html->link(__('List Exams'), ['controller' => 'Exams', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Bookedexam'), ['controller' => 'Bookedexams', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Rooms'), ['controller' => 'Rooms', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('List Wards'), ['controller' => 'Wards', 'action' => 'index']) ?></li>
                    <?php if($user['medicalrecord_id'] != null){ ?>
                        <li><?= $this->Html->link(__('View Medicalrecord'), ['controller' => 'Medicalrecords', 'action' => 'view',$user['medicalrecord_id']]) ?></li>
                    <?php } ?>
            </ul>
        </ul>
    </div>
</div>
<?php $this->end(); ?>