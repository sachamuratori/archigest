<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Html->link(__('New Users Medicalrecord'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Medicalrecord'), ['controller' => 'Medicalrecords', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersMedicalrecords index large-9 medium-8 columns content">
    <h3><?= __('Users Medicalrecords') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('medicalrecord_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersMedicalrecords as $usersMedicalrecord): ?>
            <tr>
                <td><?= $this->Number->format($usersMedicalrecord->id) ?></td>
                <td><?= $usersMedicalrecord->has('user') ? $this->Html->link($usersMedicalrecord->user->name, ['controller' => 'Users', 'action' => 'view', $usersMedicalrecord->user->id]) : '' ?></td>
                <td><?= $usersMedicalrecord->has('medicalrecord') ? $this->Html->link($usersMedicalrecord->medicalrecord->name, ['controller' => 'Medicalrecords', 'action' => 'view', $usersMedicalrecord->medicalrecord->id]) : '' ?></td>
                <td><?= h($usersMedicalrecord->created) ?></td>
                <td><?= h($usersMedicalrecord->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $usersMedicalrecord->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $usersMedicalrecord->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $usersMedicalrecord->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersMedicalrecord->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
