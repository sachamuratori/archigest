<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Welcome Admin') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $usersMedicalrecord->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $usersMedicalrecord->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Users Medicalrecords'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Medicalrecords'), ['controller' => 'Medicalrecords', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Medicalrecord'), ['controller' => 'Medicalrecords', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="usersMedicalrecords form large-9 medium-8 columns content">
    <?= $this->Form->create($usersMedicalrecord) ?>
    <fieldset>
        <legend><?= __('Edit Users Medicalrecord') ?></legend>
        <?php
            echo $this->Form->input('employeers_ids', ['options' => $users]);
            echo $this->Form->input('medicalrecords_id', ['options' => $medicalrecords]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
